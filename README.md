<img src="https://gitlab.com/joaodarcy/perttool/raw/master/src/drawable/logo_pert_tool.png" width="146" alt="PerTool">
=======
<img src="https://gitlab.com/joaodarcy/perttool/raw/master/src/drawable/logo_ufg.png" width="159" alt="UFG - Regional Jataí">&nbsp;&nbsp;<img src="https://gitlab.com/joaodarcy/perttool/raw/master/src/drawable/logo_ufla.png" width="159" alt="UFLA">
=======
Ferramenta educacional para manipulação de redes PERT/CPM

[![build status](https://gitlab.com/joaodarcy/perttool/badges/master/build.svg)](https://gitlab.com/joaodarcy/perttool/commits/master)

----------

Projeto desenvolvido como [desafio](../Desafio1.pdf) da disciplina de Projeto de Software da UFG 2014-01

----------

No âmbito do desenvolvimento de software, sabe-se que, em geral, a precisão nos cronogramas é mais importante do que a precisão nos custos [1]. Um argumento a favor dessa afirmação é que custos adicionais podem ser absorvidos por várias vendas; e em alguns casos, pode-se estipular um novo preço. Já o não cumprimento do cronograma pode: (i) reduzir o impacto no mercado; (ii) criar insatisfação dos clientes; e (iii) criar problemas com a integração dos sistemas. Rede PERT-CPM ou Rede de Planejamento é a representação gráfica de um cronograma, na qual se apresenta a sequência lógica e as interdependências entre as tarefas. São utilizadas para sistematizar o processo de elaboração e acompanhamento de cronogramas. Redes PERT-CPM podem ser aplicadas em tudo que se possa imaginar que tenha uma origem e um término previamente fixado. Desde a fabricação de um alfinete até a elaboração de um projeto para colocar um satélite em órbita.

----------

Downloads ( Última Atualização: 26/10/2017 )
===========

Binary Version : https://www.dropbox.com/s/m7qxr1scuupbfpx/PERTool%20Release%2026.10.17.zip?dl=0

----------
Rede PERT-CPM
===========

<img src="https://gitlab.com/joaodarcy/perttool/raw/master/img/rede.png" alt="PERT-CPM">

----------
Exportação para Gantt
===========

<img src="https://gitlab.com/joaodarcy/perttool/raw/master/img/gantt.png" alt="Gantt">

----------
Authors:
========

* First Version
 * Flaviane Vicente Fonseca Tinoco Sant'Anna - <flaviane.vicente@gmail.com>
 * João Darcy Tinoco Sant'Anna Neto - <jdtsncomp@gmail.com>
 * Leandro Lemes Moraes - <leandrolm3info@gmail.com>

* New Version
 * Ana Carolina Gondim Inocêncio - <anacarolina.inocencio@gmail.com>
 * Diego Bevilaqua - <diego.bevilaqua86@gmail.com>
 * Flaviane Vicente Fonseca Tinoco Sant'Anna - <flaviane.vicente@gmail.com>
 * Heitor Costa - <heitor@dcc.ufla.br>
 * João Darcy Tinoco Sant'Anna Neto - <jdtsncomp@gmail.com>
 * Paulo Afonso Parreira Júnior - <pauloapfjunior@gmail.com>
 * Paulo Henrique Lima Oliveira - <ph.lima.tecnico@gmail.com>

----------
License:
========

This software is dual-licensed to the public domain and under the following license: you are granted a perpetual, irrevocable license to copy, modify, publish and distribute this file as you see fit.

----------

[1]: Sommerville, I. "Engenharia de Software", 9a. Edição. Pearson. 554p. 2011.
