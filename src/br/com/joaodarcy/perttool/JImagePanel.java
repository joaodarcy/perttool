package br.com.joaodarcy.perttool;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;

/**
 * @author João Darcy Tinoco Sant´Anna Neto
 */
public class JImagePanel extends JPanel {
    
    private Image image;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public void paint(Graphics g) {
        if(image == null){
            super.paint(g);
        }else{
            g.drawImage(
                image.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH), 
                0, 
                0, 
                getWidth(), 
                getHeight(), 
                this
            );
        }
    }      
}
