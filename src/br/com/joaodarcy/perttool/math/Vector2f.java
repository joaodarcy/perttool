/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.math;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class Vector2f {
    
    public final static Vector2f X_AXIS = new Vector2f(1.0f, 0.0f);
    public final static Vector2f Y_AXIS = new Vector2f(0.0f, 1.0f);
    
    public float x;
    public float y;

    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }
    
    public Vector2f(final Vector2f other){
        this.x = other.x;
        this.y = other.y;
    }
    
    public void set(final Vector2f other){
        x = other.x;
        y = other.y;
    }
    
    public Vector2f add(final Vector2f other){
        return new Vector2f(x + other.x, y + other.y);
    }
    
    public Vector2f subtract(final Vector2f other){
        return new Vector2f(x - other.x, y - other.y);
    }
    
    public Vector2f multiply(final Vector2f other){
        return new Vector2f(x * other.x, y * other.y);
    }
    
    public Vector2f getNormalized(){
        final float length = getLength();
        return new Vector2f(x / length, y / length);
    }
    
    public float getLength(){
        return (float)Math.sqrt(x * x + y * y);
    }
    
    public float dot(final Vector2f other){
        return x * other.x + y * other.y;
    }
    
    public Vector2f getMidPoint(final Vector2f other){
        return new Vector2f((x + other.x) / 2.0f, (y + other.y) / 2.0f);
    }
}
