/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.gui;

import br.com.joaodarcy.perttool.Internationalization;
import br.com.joaodarcy.perttool.ResourceManager;
import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/** 
 * TODO: Translate internationalize the error messages.
 * 
 * @author Paulo Afonso Parreira Júnior <pauloapfjunior@gmail.com>
 */
public class JDCalculator extends javax.swing.JDialog {
                
    private final FocusListener selectAllOnFocusGained = new FocusAdapter() {
        @Override
        public void focusGained(FocusEvent e) {
            Component component = e.getComponent();
            if(component instanceof JTextField){
                JTextField textField = (JTextField)component;
                textField.selectAll();
            }
        }        
    };
    
    private final KeyAdapter confirmWithEnterKey = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            // Try to calculate
            confirmAction(true);
            
            if(e.getKeyCode() == KeyEvent.VK_ENTER){                                
                // Transfer focus
                e.getComponent().transferFocus();
            }                        
        }                
    };
       
    public JDCalculator(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        this.setIconImage(ResourceManager.APP_LOGO);
        
        this.setLocationRelativeTo(null);
        
        this.setTitle(String.format("%s - %s", Internationalization.getAppTitle(), Internationalization.getCalculatorActivity()));
        
        jTFOptTime.addKeyListener(confirmWithEnterKey);
        jTFOptTime.addFocusListener(selectAllOnFocusGained);
        jTFNTime.addKeyListener(confirmWithEnterKey);
        jTFNTime.addFocusListener(selectAllOnFocusGained);
        jTFPesTime.addKeyListener(confirmWithEnterKey);
        jTFPesTime.addFocusListener(selectAllOnFocusGained);
        
        jTFOptTime.requestFocus();
    }

    private void showError(String format, Object ... paramters){
        JOptionPane.showMessageDialog(this, String.format(format, paramters), this.getTitle(), JOptionPane.ERROR_MESSAGE);
    }
        
    private boolean confirmAction(boolean ignoreError){
        double optTime, pesTime, nTime, expTime;
        String errorMessage = null;
        
        try{
            if(jTFOptTime.getText().isEmpty()){
                errorMessage = "The optimist time should be informed!";
                throw new AssertionError(errorMessage);
            }            
            if(jTFNTime.getText().isEmpty()){
                errorMessage = "The normal time should be informed!";
                throw new AssertionError(errorMessage);
            }
            if(jTFPesTime.getText().isEmpty()){
                errorMessage = "The pessimist time should be informed!";
                throw new AssertionError(errorMessage);
            }
            
            try{
                optTime = Double.parseDouble(jTFOptTime.getText().replace(',', '.'));
            }catch(Throwable t){
                errorMessage = "Invalid number for optimist time!";
                throw new AssertionError(errorMessage, t);
            }
            
            try{
                nTime = Double.parseDouble(jTFNTime.getText().replace(',', '.'));
            }catch(Throwable t){
                errorMessage = "Invalid number for normal time!";
                throw new AssertionError(errorMessage, t);
            }
            
            try{
                pesTime = Double.parseDouble(jTFPesTime.getText().replace(',', '.'));
            }catch(Throwable t){
                errorMessage = "Invalid number for pessimist time!";
                throw new AssertionError(errorMessage, t);
            }
            
            expTime = (optTime + 4 * nTime + pesTime) / 6;

            jLBExpectedTime.setText(String.format("%.2f", expTime));
            
            return true;
        } catch(Throwable e){
            if(!ignoreError){
                showError("Invalid input: %s", e.getMessage());
                e.printStackTrace();            
            }
        }
        
        jLBExpectedTime.setText(errorMessage == null ? Internationalization.getDefaultExpectedTimeMsg() : errorMessage);
        return false;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTFOptTime = new javax.swing.JTextField();
        jTFNTime = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTFPesTime = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLBExpectedTime = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText(Internationalization.getOptimisticTime() + ":");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText(Internationalization.getNormalTime() + ":");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText(Internationalization.getPessimisticTime() + ":");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(Internationalization.getExpectedTime()));

        jLBExpectedTime.setBackground(new java.awt.Color(153, 204, 255));
        jLBExpectedTime.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLBExpectedTime.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLBExpectedTime.setText(Internationalization.getDefaultExpectedTimeMsg());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLBExpectedTime, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLBExpectedTime)
        );

        jButton1.setText(Internationalization.getWordClose());
        jButton1.setPreferredSize(new java.awt.Dimension(93, 23));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTFNTime)
                    .addComponent(jTFOptTime)
                    .addComponent(jTFPesTime)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTFOptTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFNTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTFPesTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLBExpectedTime;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTFNTime;
    private javax.swing.JTextField jTFOptTime;
    private javax.swing.JTextField jTFPesTime;
    // End of variables declaration//GEN-END:variables
}
