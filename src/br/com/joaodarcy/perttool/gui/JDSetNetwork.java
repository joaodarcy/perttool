/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.gui;

import br.com.joaodarcy.perttool.Event;
import br.com.joaodarcy.perttool.Internationalization;
import br.com.joaodarcy.perttool.PertException;
import br.com.joaodarcy.perttool.PertDocument;
import br.com.joaodarcy.perttool.ResourceManager;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class JDSetNetwork extends javax.swing.JDialog {

    private boolean confirmed;
    
    private final PertDocument network;
    private final KeyAdapter confirmWithEnterKey = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER){
                confirmAction();
            }
        }                
    };
    
    /**
     * Creates new form JDConfigurarRede
     */
    public JDSetNetwork(PertDocument network, java.awt.Frame parent, boolean modal) {
        super(parent, modal);                        
        initComponents();  
        
        this.confirmed = false;
        
        this.setIconImage(ResourceManager.APP_LOGO);
        
        this.jCBEventStart.addKeyListener(confirmWithEnterKey);
        this.jCBEventEnd.addKeyListener(confirmWithEnterKey);
        this.jSPQtdEvents.addKeyListener(confirmWithEnterKey);
        
        this.setLocationRelativeTo(null);
        
        this.network = network;
        
        updateGUI();
    }   

    public boolean isConfirmed() {
        return confirmed;
    }
            
    public void confirmAction(){
        this.setEnabled(false);        
        
        final int qtdEvents = (Integer)jSPQtdEvents.getValue();        
        if(qtdEvents > 0){
            final int startEventId = jCBEventStart.getSelectedIndex();                        
            if(startEventId > -1){
                final int endEventId = jCBEventEnd.getSelectedIndex();
                if(endEventId > -1){
                    try{
                        network.initialize(qtdEvents, startEventId, endEventId);
                        confirmed = true;
                        this.setVisible(false);
                    }catch(PertException e){                        
                        exibirErro("Error on the network configuration PERT: %s", e.getMessage());
                        e.printStackTrace();
                    }
                }else{
                    exibirErro("The final event must be selected!");
                    jCBEventEnd.requestFocus();
                }
            }else{
                exibirErro("The initial event must be selected!");
                jCBEventStart.requestFocus();
            }
        }else{
            exibirErro("The event count must be greater than zero!");
            jSPQtdEvents.requestFocus();
        }   
        
        this.setEnabled(true);
    }
    
    private void cleanCombobox(JComboBox combobox){
        DefaultComboBoxModel model = (DefaultComboBoxModel)combobox.getModel();
        model.removeAllElements();        
    }
    
    private void updateGUI(){
        Integer qtdEvents = (Integer)jSPQtdEvents.getValue();        
        if(qtdEvents > 0){
            int oldStart = jCBEventStart.getSelectedIndex();
            int oldEnd = jCBEventEnd.getSelectedIndex();
            
            cleanCombobox(jCBEventEnd);            
            cleanCombobox(jCBEventStart);
            
            final Event eventArray[] = new Event[qtdEvents];
            for(int i = 0 ; i < qtdEvents ; i++){
                eventArray[i] = network.newTempEvent(i);
            }
            
            jCBEventStart.setModel(new DefaultComboBoxModel<>(eventArray));
            jCBEventEnd.setModel(new DefaultComboBoxModel<>(eventArray));                                   
            
            if(qtdEvents > oldStart){
                jCBEventStart.setSelectedIndex(oldStart);
            }else{
                jCBEventStart.setSelectedIndex(-1);
            }
            
            if(qtdEvents > oldEnd){
                jCBEventEnd.setSelectedIndex(oldEnd);
            }else{
                jCBEventEnd.setSelectedIndex(-1);
            }            
        }else{
            cleanCombobox(jCBEventEnd);
            cleanCombobox(jCBEventStart);
            
            jCBEventEnd.setSelectedIndex(-1);            
            jCBEventStart.setSelectedIndex(-1);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jSPQtdEvents = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        jCBEventStart = new javax.swing.JComboBox<Event>();
        jLabel3 = new javax.swing.JLabel();
        jCBEventEnd = new javax.swing.JComboBox<Event>();
        jBTCancelar = new javax.swing.JButton();
        jBTOK = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(String.format("%s - %s", Internationalization.getAppTitle(), Internationalization.getSetupNetwork()));

        jLabel1.setText(Internationalization.getEventNum());

        jSPQtdEvents.setModel(new javax.swing.SpinnerNumberModel(PertDocument.MIN_EVENT_COUNT, PertDocument.MIN_EVENT_COUNT, PertDocument.MAX_EVENT_COUNT, 1));
        jSPQtdEvents.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSPQtdEventsStateChanged(evt);
            }
        });

        jLabel2.setText(Internationalization.getStartEvent() + ":");

        jLabel3.setText(Internationalization.getEndEvent() + ":");

        jBTCancelar.setText(Internationalization.getWordCancel());
        jBTCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTCancelarActionPerformed(evt);
            }
        });

        jBTOK.setText(Internationalization.getWordOK());
        jBTOK.setName(""); // NOI18N
        jBTOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTOKActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCBEventStart, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSPQtdEvents, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCBEventEnd, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jBTOK)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBTCancelar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jSPQtdEvents, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jCBEventStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jCBEventEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBTCancelar)
                    .addComponent(jBTOK))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jSPQtdEventsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSPQtdEventsStateChanged
        updateGUI();
    }//GEN-LAST:event_jSPQtdEventsStateChanged

    private void jBTCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTCancelarActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_jBTCancelarActionPerformed

    private void exibirErro(String format, Object ... paramters){
        JOptionPane.showMessageDialog(this, String.format(format, paramters), this.getTitle(), JOptionPane.ERROR_MESSAGE);
    }
    
    private void jBTOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTOKActionPerformed
        confirmAction();
    }//GEN-LAST:event_jBTOKActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBTCancelar;
    private javax.swing.JButton jBTOK;
    private javax.swing.JComboBox<Event> jCBEventEnd;
    private javax.swing.JComboBox<Event> jCBEventStart;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSpinner jSPQtdEvents;
    // End of variables declaration//GEN-END:variables
}
