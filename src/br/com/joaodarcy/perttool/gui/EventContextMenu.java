/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.gui;

import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.Event;
import br.com.joaodarcy.perttool.Internationalization;
import br.com.joaodarcy.perttool.cmd.MakeFinalEventCommand;
import br.com.joaodarcy.perttool.cmd.MakeInitialEventCommand;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class EventContextMenu extends JPopupMenu {
    private final Event event;
        
    private EventContextMenu(final Event event) {
        this.event = event;
        
        final JMenuItem jMIMakeFinal = new JMenuItem(Internationalization.getMakeFinalEvent());
        final JMenuItem jMIMakeInitial = new JMenuItem(Internationalization.getMakeInitialEvent());
        JMenuItem jMIEditNetworkLateTime = null;
        JMenuItem jMIDelete = null;
        
        switch(event.getType()){
            case EVENT_TYPE_START:
                jMIMakeInitial.setEnabled(false);
                break;
            case EVENT_TYPE_END:
                jMIMakeFinal.setEnabled(false);
                jMIEditNetworkLateTime = new JMenuItem(Internationalization.getOverrideLateTime());
                break;
            default:
                jMIDelete = new JMenuItem(Internationalization.getDeleteEvent());
                break;
        }
        
        jMIMakeInitial.addActionListener(new MakeInicialActionListener());        
        jMIMakeFinal.addActionListener(new MakeFinalActionListener());
        
        this.add(jMIMakeInitial);
        this.add(jMIMakeFinal);
        
        if(jMIEditNetworkLateTime != null){
            jMIEditNetworkLateTime.addActionListener(new EditNetworkLateTimeActionListener());            
            this.add(new JSeparator());
            this.add(jMIEditNetworkLateTime);
        }
        if(jMIDelete != null){
            jMIDelete.addActionListener(new DeleteEventActionListener());
            this.add(new JSeparator());            
            this.add(jMIDelete);
        }
    }
    
    private final class DeleteEventActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            event.showDeleteDialog();
        }        
    }
    
    private final class EditNetworkLateTimeActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {            
            GUIHelper.showEditNetworkLateTime();            
        }        
    }
    
    private final class MakeInicialActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {            
            if(MakeInitialEventCommand.canMakeInitial(event)){
                Application.storeAndExecute(new MakeInitialEventCommand(event.id));
            }                        
        }        
    }
    
    private final class MakeFinalActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {            
            if(MakeFinalEventCommand.canMakeFinal(event)){
                Application.storeAndExecute(new MakeFinalEventCommand(event.id));
            }
        }        
    }
    
    public static void showContextMenuFor(Event event, int x, int y){
        EventContextMenu menu = new EventContextMenu(event);                
        menu.show(Application.getDoc(), x, y);
    }    
}
