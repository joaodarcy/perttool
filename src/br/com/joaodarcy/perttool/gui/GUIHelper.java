/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.gui;

import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.PertDocument;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public final class GUIHelper {

    // TODO: Move all gui stuff here
    
    private GUIHelper() {
        throw new AssertionError();
    }            
        
    public static void showEditNetworkLateTime(){
        final PertDocument doc = Application.getDoc();
        if(doc.initialized){
            JDEditNetworkLateTime editNetworklateTimeGUI = new JDEditNetworkLateTime(doc, null, true);
            editNetworklateTimeGUI.setVisible(true);
            doc.repaint();
        }
    }
    
}
