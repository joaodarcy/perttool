/**
 * PERTTool <https://gitlab.com/joaodarcy/perttool>
 * Copyright (C) 2014-2016 João Darcy Tinoco Sant´Anna Neto
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.gui;

import br.com.joaodarcy.perttool.Activity;
import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.CommandHistoryListener;
import br.com.joaodarcy.perttool.Internationalization;
import br.com.joaodarcy.perttool.PertDocument;
import br.com.joaodarcy.perttool.ResourceManager;
import br.com.joaodarcy.perttool.cmd.AddEventCommand;
import br.com.joaodarcy.perttool.desenhar.Constants;
import static br.com.joaodarcy.perttool.ui.DrawUtils.*;
import br.com.joaodarcy.perttool.ui.UIElement;
import br.com.joaodarcy.perttool.ui.UIListener;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.Locale;
import java.util.Set;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.filechooser.FileNameExtensionFilter;

public class JFPertTool extends javax.swing.JFrame {

    private final PertDocument network;
    private final Dimension preferedSize;

    private File workingFile;
    private String exampleFile;
    private String title;

    private final static LanguageChange SUPPORTED_LANGUAGE_ARRAY[] = new LanguageChange[]{
        new LanguageChange(new Locale("pt", "BR"), ResourceManager.LANG_PT_BR, "word.portuguese"),
        new LanguageChange(Locale.ENGLISH, ResourceManager.LANG_EN_US, "word.english"),
        new LanguageChange(new Locale("es"), ResourceManager.LANG_ES, "word.spanish")
    };

    // TODO: Remove old listeners !
    private final static class LanguageChange implements ActionListener {

        private final Locale locale;
        private final ImageIcon imageIcon;
        private final String i18nEntry;

        private JFPertTool guiInstance;

        public LanguageChange(Locale locale, ImageIcon imageIcon, String i18nEntry) {
            this.locale = locale;
            this.imageIcon = imageIcon;
            this.i18nEntry = i18nEntry;
        }

        public void setGuiInstance(JFPertTool guiInstance) {
            this.guiInstance = guiInstance;
        }

        public String getI18nEntry() {
            return i18nEntry;
        }

        public ImageIcon getImageIcon() {
            return imageIcon;
        }

        public Locale getLocale() {
            return locale;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Internationalization.changeLocale(locale);
            JFPertTool newGUI = new JFPertTool(guiInstance);            
            newGUI.setVisible(true);
        }
    }

    private final class ExampleLoader implements ActionListener {

        private final String fileName;

        public ExampleLoader(String fileName) {
            this.fileName = fileName;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            openExampleFile(fileName);
        }
    }

    private final CommandHistoryListener commandHistoryListener = new CommandHistoryListener() {
        @Override
        public void undoAvailable(boolean available) {
            jMIUndo.setEnabled(available);
            jBTNUndo.setEnabled(available);
        }

        @Override
        public void redoAvailable(boolean available) {
            jMIRedo.setEnabled(available);
            jBTNRedo.setEnabled(available);
        }
    };

    public JFPertTool(JFPertTool other) {
        this.network = Application.getDoc();
        this.preferedSize = new Dimension(other.getWidth(), other.getHeight());

        other.setVisible(false);        

        this.initialize();

        // Restore the old state
        if (network.initialized) {
            this.workingFile = other.workingFile;
            this.exampleFile = other.exampleFile;
            if(other.exampleFile == null || Application.isUndoAvailable()){
                this.updateWorkingFile(other.workingFile, other.title, other.jBTSave.isEnabled());                
            }else{
                this.openExampleFile(other.exampleFile);
            }
            this.onUISelectedItem(network.getUiSystem().getFocused());
            this.jSPFontSize.setValue(other.jSPFontSize.getValue());
            this.jTBHighQuality.setSelected(other.jTBHighQuality.isSelected());
            this.jTBLegend.setSelected(other.jTBLegend.isSelected());
        }
        network.getUiSystem().setViewDirty();

        other.dispose();
    }

    /**
     * Creates new form JFPertTool
     */
    public JFPertTool() {
        this.network = Application.getDoc();
        this.preferedSize = new Dimension(800, 600);
        this.exampleFile = null;
        this.initialize();
    }

    private void initialize() {
        initComponents();

        // Register Command History Listener
        Application.setCommandHistoryListener(commandHistoryListener);

        // Maximize the screen
        this.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);

        this.setLocationRelativeTo(null);
        updateWorkingFile(null);

        this.setIconImage(ResourceManager.APP_LOGO);

        jBTNew.setIcon(ResourceManager.ICON_FILE);
        jBTOpen.setIcon(ResourceManager.ICON_FOLDER_OPEN);
        jBTSave.setIcon(ResourceManager.ICON_SAVE);

        jLBFontSize.setIcon(ResourceManager.ICON_FONT);
        jTBHighQuality.setIcon(ResourceManager.ICON_PAINT_BRUSH);
        jTBLegend.setIcon(ResourceManager.ICON_INFO_CIRCLE);

        jBTNOverrideLateTime.setIcon(ResourceManager.ICON_CLOCK);
        jBTNCalculator.setIcon(ResourceManager.ICON_CALCULATOR);
        jBTEditActivity.setIcon(ResourceManager.ICON_EDIT);
        jBTDeleteActivity.setIcon(ResourceManager.ICON_TRASH);

        jBTNEvaluation.setIcon(ResourceManager.ICON_EVALUATION);

        network.addUIListener(new UIListener() {
            @Override
            public void onSelectedItem(UIElement element) {
                onUISelectedItem(element);
            }
        });

        // Add Language Buttons
        {
            final Locale defaultLocale = Locale.getDefault();

            for (LanguageChange lang : SUPPORTED_LANGUAGE_ARRAY) {
                lang.setGuiInstance(this);

                JButton languageButton = new JButton();
                languageButton.setIcon(lang.getImageIcon());
                languageButton.setToolTipText(Internationalization.getMessage(lang.getI18nEntry()));
                languageButton.setFocusable(false);

                if (lang.getLocale().equals(defaultLocale)) {
                    languageButton.setEnabled(false);
                }
                languageButton.addActionListener(lang);

                jToolBar1.add(languageButton);
            }

            // Add a last separator to Toolbar
            jToolBar1.add(new JSeparator(JSeparator.VERTICAL));
        }

        // Load Example Files
        Set<Object> exampleFiles = Internationalization.getExampleFileList();
        for (Object fileNameObj : exampleFiles) {
            String fileName = fileNameObj + ".ptf";
            String name = Internationalization.getExampleFileName(fileNameObj);

            JMenuItem exampleItem = new JMenuItem();
            exampleItem.setText(name);
            exampleItem.addActionListener(new ExampleLoader(fileName));
            jMNExamples.add(exampleItem);
        }

        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                if (network.initialized) {
                    network.repaint();
                }
                // Development Only
                //openExampleFile("rede-cafe.ptf");
            }
        });
        
        ButtonGroup connectorType = new ButtonGroup();
        connectorType.add(jTBConnectorLine);
        connectorType.add(jTBConnectorCurve);
        
        if(Constants.DEFAULT_CONNECTOR == ConnectorType.CONNECTOR_TYPE_LINE){
            jTBConnectorLine.setSelected(true);
        }else{
            jTBConnectorCurve.setSelected(true);
        }
    }

    private void onUISelectedItem(UIElement element) {
        if (Activity.isActivity(element)) {
            jBTEditActivity.setEnabled(true);
            jBTDeleteActivity.setEnabled(true);
        } else {
            jBTEditActivity.setEnabled(false);
            jBTDeleteActivity.setEnabled(false);
        }
    }

    private void buildDevExample() {
        try {
            network.initialize(3, 0, 1);

            network.addActivity(10, 0, 1);
            network.addActivity(0, 0, 2); // Phantom

            int redeHeight = jPNPreview.getHeight();

            network.eventMap.get(0).getParent().setPosition(100, redeHeight / 2);
            network.eventMap.get(1).getParent().setPosition(300, redeHeight / 4);
            network.eventMap.get(2).getParent().setPosition(300, redeHeight - redeHeight / 4);

            network.repaint();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator5 = new javax.swing.JSeparator();
        jSeparator10 = new javax.swing.JSeparator();
        jSeparator12 = new javax.swing.JSeparator();
        jToolBar1 = new javax.swing.JToolBar();
        jBTNew = new javax.swing.JButton();
        jBTOpen = new javax.swing.JButton();
        jBTSave = new javax.swing.JButton();
        jSeparator11 = new javax.swing.JToolBar.Separator();
        jBTNUndo = new javax.swing.JButton();
        jBTNRedo = new javax.swing.JButton();
        jSeparator16 = new javax.swing.JToolBar.Separator();
        jBTNAddEvent = new javax.swing.JButton();
        jBTNOverrideLateTime = new javax.swing.JButton();
        jBTNCalculator = new javax.swing.JButton();
        jSeparator13 = new javax.swing.JToolBar.Separator();
        jBTEditActivity = new javax.swing.JButton();
        jBTDeleteActivity = new javax.swing.JButton();
        jSeparator9 = new javax.swing.JToolBar.Separator();
        jLBFontSize = new javax.swing.JLabel();
        jSPFontSize = new javax.swing.JSpinner();
        jSeparator15 = new javax.swing.JToolBar.Separator();
        jLabel1 = new javax.swing.JLabel();
        jSPDecimalPlaces = new javax.swing.JSpinner();
        jSeparator14 = new javax.swing.JToolBar.Separator();
        jTBHighQuality = new javax.swing.JToggleButton();
        jTBLegend = new javax.swing.JToggleButton();
        jSeparator6 = new javax.swing.JToolBar.Separator();
        jBTNEvaluation = new javax.swing.JButton();
        jSeparator17 = new javax.swing.JToolBar.Separator();
        jTBConnectorLine = new javax.swing.JToggleButton();
        jTBConnectorCurve = new javax.swing.JToggleButton();
        jSeparator18 = new javax.swing.JToolBar.Separator();
        jPanel0 = new javax.swing.JPanel();
        jPNPreview = Application.getDoc();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMINew = new javax.swing.JMenuItem();
        jMIOpen = new javax.swing.JMenuItem();
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        jMNExamples = new javax.swing.JMenu();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMISave = new javax.swing.JMenuItem();
        jMISaveAs = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMIPrint = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jMenu3 = new javax.swing.JMenu();
        jMIExport = new javax.swing.JMenuItem();
        jMIGantt = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMIExit = new javax.swing.JMenuItem();
        jMNEdit = new javax.swing.JMenu();
        jMIUndo = new javax.swing.JMenuItem();
        jMIRedo = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMIHelpContent = new javax.swing.JMenuItem();
        jMIEvaluate = new javax.swing.JMenuItem();
        jSeparator7 = new javax.swing.JPopupMenu.Separator();
        jMIAbout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocationByPlatform(true);
        setMaximumSize(preferedSize);
        setMinimumSize(preferedSize);
        setPreferredSize(preferedSize);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setMaximumSize(new java.awt.Dimension(32769, 32769));
        jToolBar1.setMinimumSize(new java.awt.Dimension(0, 30));
        jToolBar1.setPreferredSize(new java.awt.Dimension(0, 30));

        jBTNew.setToolTipText(Internationalization.getWordNew());
        jBTNew.setFocusable(false);
        jBTNew.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTNew.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTNew.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTNew.setPreferredSize(new java.awt.Dimension(28, 28));
        jBTNew.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTNewActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTNew);

        jBTOpen.setToolTipText(Internationalization.getWordOpen());
        jBTOpen.setFocusable(false);
        jBTOpen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTOpen.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTOpen.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTOpen.setPreferredSize(new java.awt.Dimension(28, 28));
        jBTOpen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTOpenActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTOpen);

        jBTSave.setToolTipText(Internationalization.getWordSave());
        jBTSave.setEnabled(false);
        jBTSave.setFocusable(false);
        jBTSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTSave.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTSave.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTSave.setPreferredSize(new java.awt.Dimension(28, 28));
        jBTSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTSaveActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTSave);
        jToolBar1.add(jSeparator11);

        jBTNUndo.setIcon(ResourceManager.ICON_UNDO);
        jBTNUndo.setToolTipText(Internationalization.getWordUndo());
        jBTNUndo.setFocusable(false);
        jBTNUndo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTNUndo.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTNUndo.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTNUndo.setPreferredSize(new java.awt.Dimension(28, 28));
        jBTNUndo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTNUndo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTNUndoActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTNUndo);

        jBTNRedo.setIcon(ResourceManager.ICON_REDO);
        jBTNRedo.setToolTipText(Internationalization.getWordRedo());
        jBTNRedo.setFocusable(false);
        jBTNRedo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTNRedo.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTNRedo.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTNRedo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTNRedo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTNRedoActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTNRedo);
        jToolBar1.add(jSeparator16);

        jBTNAddEvent.setIcon(ResourceManager.ICON_ADD_EVENT);
        jBTNAddEvent.setToolTipText(Internationalization.getAddNewEvent());
        jBTNAddEvent.setEnabled(false);
        jBTNAddEvent.setFocusable(false);
        jBTNAddEvent.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTNAddEvent.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTNAddEvent.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTNAddEvent.setPreferredSize(new java.awt.Dimension(28, 28));
        jBTNAddEvent.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTNAddEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTNAddEventActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTNAddEvent);

        jBTNOverrideLateTime.setToolTipText(Internationalization.getOverrideLateTime());
        jBTNOverrideLateTime.setEnabled(false);
        jBTNOverrideLateTime.setFocusable(false);
        jBTNOverrideLateTime.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTNOverrideLateTime.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTNOverrideLateTime.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTNOverrideLateTime.setPreferredSize(new java.awt.Dimension(28, 28));
        jBTNOverrideLateTime.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTNOverrideLateTime.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTNOverrideLateTimeActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTNOverrideLateTime);

        jBTNCalculator.setToolTipText(Internationalization.getCalculatorActivity());
        jBTNCalculator.setEnabled(false);
        jBTNCalculator.setFocusable(false);
        jBTNCalculator.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTNCalculator.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTNCalculator.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTNCalculator.setPreferredSize(new java.awt.Dimension(28, 28));
        jBTNCalculator.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTNCalculator.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTNCalculatorActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTNCalculator);
        jToolBar1.add(jSeparator13);

        jBTEditActivity.setToolTipText(Internationalization.getEditActivity());
        jBTEditActivity.setEnabled(false);
        jBTEditActivity.setFocusable(false);
        jBTEditActivity.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTEditActivity.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTEditActivity.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTEditActivity.setPreferredSize(new java.awt.Dimension(28, 28));
        jBTEditActivity.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTEditActivity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTEditActivityActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTEditActivity);

        jBTDeleteActivity.setToolTipText(Internationalization.getConfirmDeleteActivityTitle());
        jBTDeleteActivity.setEnabled(false);
        jBTDeleteActivity.setFocusable(false);
        jBTDeleteActivity.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTDeleteActivity.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTDeleteActivity.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTDeleteActivity.setPreferredSize(new java.awt.Dimension(28, 28));
        jBTDeleteActivity.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTDeleteActivity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTDeleteActivityActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTDeleteActivity);
        jToolBar1.add(jSeparator9);

        jLBFontSize.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLBFontSize.setToolTipText(Internationalization.getFontSize());
        jLBFontSize.setMaximumSize(new java.awt.Dimension(28, 28));
        jLBFontSize.setMinimumSize(new java.awt.Dimension(28, 28));
        jLBFontSize.setPreferredSize(new java.awt.Dimension(28, 28));
        jToolBar1.add(jLBFontSize);

        jSPFontSize.setModel(new javax.swing.SpinnerNumberModel(12, 12, 42, 1));
        jSPFontSize.setToolTipText(Internationalization.getFontSize());
        jSPFontSize.setEnabled(false);
        jSPFontSize.setMaximumSize(new java.awt.Dimension(39, 32767));
        jSPFontSize.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSPFontSizeStateChanged(evt);
            }
        });
        jToolBar1.add(jSPFontSize);
        jToolBar1.add(jSeparator15);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(ResourceManager.ICON_DECIMAL_PLACES);
        jLabel1.setToolTipText(Internationalization.getDecimalPlaces());
        jLabel1.setMaximumSize(new java.awt.Dimension(28, 28));
        jLabel1.setMinimumSize(new java.awt.Dimension(28, 28));
        jLabel1.setPreferredSize(new java.awt.Dimension(28, 28));
        jToolBar1.add(jLabel1);

        jSPDecimalPlaces.setModel(new javax.swing.SpinnerNumberModel(network.getDecimalPlaces(), PertDocument.MIN_DECIMAL_PLACES, PertDocument.MAX_DECIMAL_PLACES, 1));
        jSPDecimalPlaces.setToolTipText(Internationalization.getDecimalPlaces());
        jSPDecimalPlaces.setEnabled(false);
        jSPDecimalPlaces.setMaximumSize(new java.awt.Dimension(39, 28));
        jSPDecimalPlaces.setMinimumSize(new java.awt.Dimension(39, 28));
        jSPDecimalPlaces.setPreferredSize(new java.awt.Dimension(39, 28));
        jSPDecimalPlaces.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSPDecimalPlacesStateChanged(evt);
            }
        });
        jToolBar1.add(jSPDecimalPlaces);
        jToolBar1.add(jSeparator14);

        jTBHighQuality.setSelected(true);
        jTBHighQuality.setToolTipText(Internationalization.getHighQuality());
        jTBHighQuality.setEnabled(false);
        jTBHighQuality.setFocusable(false);
        jTBHighQuality.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jTBHighQuality.setMaximumSize(new java.awt.Dimension(28, 28));
        jTBHighQuality.setMinimumSize(new java.awt.Dimension(28, 28));
        jTBHighQuality.setPreferredSize(new java.awt.Dimension(28, 28));
        jTBHighQuality.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jTBHighQuality.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTBHighQualityActionPerformed(evt);
            }
        });
        jToolBar1.add(jTBHighQuality);

        jTBLegend.setSelected(true);
        jTBLegend.setToolTipText(Internationalization.getWordLegend()
        );
        jTBLegend.setEnabled(false);
        jTBLegend.setFocusable(false);
        jTBLegend.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jTBLegend.setMaximumSize(new java.awt.Dimension(28, 28));
        jTBLegend.setMinimumSize(new java.awt.Dimension(28, 28));
        jTBLegend.setPreferredSize(new java.awt.Dimension(28, 28));
        jTBLegend.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jTBLegend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTBLegendActionPerformed(evt);
            }
        });
        jToolBar1.add(jTBLegend);
        jToolBar1.add(jSeparator6);

        jBTNEvaluation.setToolTipText(Internationalization.getWordEvaluation());
        jBTNEvaluation.setFocusable(false);
        jBTNEvaluation.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBTNEvaluation.setMaximumSize(new java.awt.Dimension(28, 28));
        jBTNEvaluation.setMinimumSize(new java.awt.Dimension(28, 28));
        jBTNEvaluation.setPreferredSize(new java.awt.Dimension(28, 28));
        jBTNEvaluation.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBTNEvaluation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTNEvaluationActionPerformed(evt);
            }
        });
        jToolBar1.add(jBTNEvaluation);
        jToolBar1.add(jSeparator17);

        jTBConnectorLine.setIcon(ResourceManager.ICON_STRAIGHT_LINE);
        jTBConnectorLine.setToolTipText(Internationalization.getConnectorTypeLine());
        jTBConnectorLine.setFocusable(false);
        jTBConnectorLine.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jTBConnectorLine.setMaximumSize(new java.awt.Dimension(28, 28));
        jTBConnectorLine.setMinimumSize(new java.awt.Dimension(28, 28));
        jTBConnectorLine.setPreferredSize(new java.awt.Dimension(28, 28));
        jTBConnectorLine.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jTBConnectorLine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTBConnectorLineActionPerformed(evt);
            }
        });
        jToolBar1.add(jTBConnectorLine);

        jTBConnectorCurve.setIcon(ResourceManager.ICON_CUBIC_BEZIER);
        jTBConnectorCurve.setToolTipText(Internationalization.getConnectorTypeCurve());
        jTBConnectorCurve.setFocusable(false);
        jTBConnectorCurve.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jTBConnectorCurve.setMaximumSize(new java.awt.Dimension(28, 28));
        jTBConnectorCurve.setMinimumSize(new java.awt.Dimension(28, 28));
        jTBConnectorCurve.setPreferredSize(new java.awt.Dimension(28, 28));
        jTBConnectorCurve.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jTBConnectorCurve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTBConnectorCurveActionPerformed(evt);
            }
        });
        jToolBar1.add(jTBConnectorCurve);
        jToolBar1.add(jSeparator18);

        jPanel0.setBorder(javax.swing.BorderFactory.createTitledBorder(Internationalization.getWordPreview()));

        jPNPreview.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPNPreviewLayout = new javax.swing.GroupLayout(jPNPreview);
        jPNPreview.setLayout(jPNPreviewLayout);
        jPNPreviewLayout.setHorizontalGroup(
            jPNPreviewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPNPreviewLayout.setVerticalGroup(
            jPNPreviewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 581, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel0Layout = new javax.swing.GroupLayout(jPanel0);
        jPanel0.setLayout(jPanel0Layout);
        jPanel0Layout.setHorizontalGroup(
            jPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel0Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPNPreview, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel0Layout.setVerticalGroup(
            jPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel0Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPNPreview, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jMenu1.setText(Internationalization.getWordFile());

        jMINew.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMINew.setIcon(ResourceManager.ICON_FILE);
        jMINew.setText(Internationalization.getWordNew());
        jMINew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMINewActionPerformed(evt);
            }
        });
        jMenu1.add(jMINew);

        jMIOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMIOpen.setIcon(ResourceManager.ICON_FOLDER_OPEN);
        jMIOpen.setText(Internationalization.getWordOpen());
        jMIOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIOpenActionPerformed(evt);
            }
        });
        jMenu1.add(jMIOpen);
        jMenu1.add(jSeparator8);

        jMNExamples.setText(Internationalization.getWordExample());
        jMenu1.add(jMNExamples);
        jMenu1.add(jSeparator2);

        jMISave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMISave.setIcon(ResourceManager.ICON_SAVE);
        jMISave.setText(Internationalization.getWordSave());
        jMISave.setEnabled(false);
        jMISave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMISaveActionPerformed(evt);
            }
        });
        jMenu1.add(jMISave);

        jMISaveAs.setText(Internationalization.getWordSaveAs());
        jMISaveAs.setEnabled(false);
        jMISaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMISaveAsActionPerformed(evt);
            }
        });
        jMenu1.add(jMISaveAs);
        jMenu1.add(jSeparator1);

        jMIPrint.setText(Internationalization.getWordPrint());
        jMIPrint.setEnabled(false);
        jMenu1.add(jMIPrint);
        jMenu1.add(jSeparator3);

        jMenu3.setText(Internationalization.getWordExport());

        jMIExport.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMIExport.setText(Internationalization.getPNGExport());
        jMIExport.setEnabled(false);
        jMIExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIExportActionPerformed(evt);
            }
        });
        jMenu3.add(jMIExport);

        jMIGantt.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        jMIGantt.setText(Internationalization.getGanttExport());
        jMIGantt.setEnabled(false);
        jMIGantt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIGanttActionPerformed(evt);
            }
        });
        jMenu3.add(jMIGantt);

        jMenu1.add(jMenu3);
        jMenu1.add(jSeparator4);

        jMIExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        jMIExit.setText(Internationalization.getWordExit());
        jMIExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIExitActionPerformed(evt);
            }
        });
        jMenu1.add(jMIExit);

        jMenuBar1.add(jMenu1);

        jMNEdit.setText(Internationalization.getWordEdit());

        jMIUndo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        jMIUndo.setIcon(ResourceManager.ICON_UNDO);
        jMIUndo.setText(Internationalization.getWordUndo());
        jMIUndo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIUndoActionPerformed(evt);
            }
        });
        jMNEdit.add(jMIUndo);

        jMIRedo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Y, java.awt.event.InputEvent.CTRL_MASK));
        jMIRedo.setIcon(ResourceManager.ICON_REDO);
        jMIRedo.setText(Internationalization.getWordRedo());
        jMIRedo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIRedoActionPerformed(evt);
            }
        });
        jMNEdit.add(jMIRedo);

        jMenuBar1.add(jMNEdit);

        jMenu2.setText(Internationalization.getWordHelp());

        jMIHelpContent.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMIHelpContent.setText(Internationalization.getWordHelp());
        jMIHelpContent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIHelpContentActionPerformed(evt);
            }
        });
        jMenu2.add(jMIHelpContent);

        jMIEvaluate.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        jMIEvaluate.setIcon(ResourceManager.ICON_EVALUATION);
        jMIEvaluate.setText(Internationalization.getWordEvaluation());
        jMIEvaluate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIEvaluateActionPerformed(evt);
            }
        });
        jMenu2.add(jMIEvaluate);
        jMenu2.add(jSeparator7);

        jMIAbout.setText(Internationalization.getWordAbout());
        jMIAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIAboutActionPerformed(evt);
            }
        });
        jMenu2.add(jMIAbout);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
            .addComponent(jPanel0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jSPFontSizeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSPFontSizeStateChanged
        network.setFontSize((Integer) jSPFontSize.getValue());
        network.repaint();
    }//GEN-LAST:event_jSPFontSizeStateChanged

    private void onNetworkInitialized(boolean supportSave) {
        jMISave.setEnabled(supportSave);
        jBTSave.setEnabled(supportSave);
        jMISaveAs.setEnabled(true);
        jMIExport.setEnabled(true);
        jMIGantt.setEnabled(true);
        jBTNOverrideLateTime.setEnabled(true);
        jBTNAddEvent.setEnabled(true);
        jSPDecimalPlaces.setEnabled(true);
        jSPFontSize.setEnabled(true);
        jBTNCalculator.setEnabled(true);
        jTBHighQuality.setEnabled(true);
        jTBLegend.setEnabled(true);
    }

    private void newAction() {
        JDSetNetwork initializeNetworkGUI = new JDSetNetwork(network, this, true);
        initializeNetworkGUI.setVisible(true);

        if (initializeNetworkGUI.isConfirmed()) {
            Application.clearCommandHistory();
            updateWorkingFile(null, Internationalization.getUntitled());
            network.repaint();
        }
    }

    private void jMINewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMINewActionPerformed
        newAction();
    }//GEN-LAST:event_jMINewActionPerformed

    private void jMIExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMIExitActionPerformed

    private void exportTo(ExportFormat format){
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = null;
        switch(format){
            case PERT_PNG:
                filter = new FileNameExtensionFilter("PNG Image", "png");
                break;
            case GANTT_PNG:
                filter = new FileNameExtensionFilter("Gantt PNG Image", "png");
                break;
        }
        fileChooser.addChoosableFileFilter(filter);
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setAcceptAllFileFilterUsed(false);

        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File fileOut = fileChooser.getSelectedFile();
            if (!fileOut.getName().toLowerCase().endsWith("png")) {
                fileOut = new File(fileOut.getPath() + ".png");
            }
            if (network.exportImage(fileOut, format)) {
                JOptionPane.showMessageDialog(this, Internationalization.getExportImgSuccess(), Internationalization.getAppTitle(), JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, Internationalization.getExportImgError(), Internationalization.getAppTitle(), JOptionPane.ERROR_MESSAGE);
            }
            network.repaint();
        }
    }
    
    private void jMIExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIExportActionPerformed
        exportTo(ExportFormat.PERT_PNG);
    }//GEN-LAST:event_jMIExportActionPerformed

    private void saveProject(boolean saveAs) {
        if (!network.initialized) {
            JOptionPane.showMessageDialog(this, Internationalization.getSaveErrorDiagramEmpty(), Internationalization.getAppTitle(), JOptionPane.ERROR_MESSAGE);
            network.repaint();
            return;
        }

        boolean shouldSave = true;
        File fileOut = workingFile;

        if (saveAs || workingFile == null) {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("PERTool File", "ptf");
            fileChooser.addChoosableFileFilter(filter);
            fileChooser.setMultiSelectionEnabled(false);
            fileChooser.setAcceptAllFileFilterUsed(false);

            shouldSave = (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION);
            if (shouldSave) {
                fileOut = fileChooser.getSelectedFile();
            }
        }

        if (shouldSave) {
            if (!fileOut.getName().toLowerCase().endsWith("ptf")) {
                fileOut = new File(fileOut.getPath() + ".ptf");
            }
            if (network.save(fileOut)) {
                updateWorkingFile(fileOut);
                //JOptionPane.showMessageDialog(this, Internationalization.getSaveSuccess(), Internationalization.getAppTitle(), JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, Internationalization.getSaveError(), Internationalization.getAppTitle(), JOptionPane.ERROR_MESSAGE);
            }
            network.repaint();
        }
    }

    private void jMISaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMISaveActionPerformed
        saveProject(false);
    }//GEN-LAST:event_jMISaveActionPerformed

    private void openExampleFile(String exampleFile) {
        InputStream is = JFPertTool.class.getClassLoader().getResourceAsStream("example/" + exampleFile);
        if (network.load(is)) {
            this.exampleFile = exampleFile;
            Application.clearCommandHistory();
            updateWorkingFile(null, exampleFile, false);
        } else {
            JOptionPane.showMessageDialog(this, Internationalization.getOpenError(), Internationalization.getAppTitle(), JOptionPane.ERROR_MESSAGE);
        }
        network.repaint();
    }

    private void openActionShowPrompt() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setAcceptAllFileFilterUsed(false);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("PERTool File", "ptf");
        fileChooser.addChoosableFileFilter(filter);
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            this.exampleFile = null;
            if (network.load(fileChooser.getSelectedFile())) {
                Application.clearCommandHistory();
                updateWorkingFile(fileChooser.getSelectedFile());
            } else {
                JOptionPane.showMessageDialog(this, Internationalization.getOpenError(), Internationalization.getAppTitle(), JOptionPane.ERROR_MESSAGE);
            }
            network.repaint();
        }
    }

    private void jMIOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIOpenActionPerformed
        openActionShowPrompt();
    }//GEN-LAST:event_jMIOpenActionPerformed

    private void updateWorkingFile(File newWorkingFile) {
        updateWorkingFile(newWorkingFile, null);
    }

    private void updateWorkingFile(File newWorkingFile, String untitledName) {
        updateWorkingFile(newWorkingFile, untitledName, true);
    }

    private void updateWorkingFile(File newWorkingFile, String untitledName, boolean supportSave) {
        this.workingFile = newWorkingFile;
        this.title = untitledName == null ? "" : untitledName;
        
        if(untitledName == null){
            setTitle(Internationalization.getAppTitle());
        }else{
            setTitle(
                String.format(
                    "%s :: %s", 
                    Internationalization.getAppTitle(), 
                    this.workingFile == null ? untitledName : newWorkingFile.getPath()
                )
            );
        }
        if (network.initialized) {
            jSPDecimalPlaces.getModel().setValue(network.getDecimalPlaces());
            onNetworkInitialized(supportSave);
        }
    }

    private void jMISaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMISaveAsActionPerformed
        saveProject(true);
    }//GEN-LAST:event_jMISaveAsActionPerformed

    private void jBTNOverrideLateTimeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTNOverrideLateTimeActionPerformed
        GUIHelper.showEditNetworkLateTime();
    }//GEN-LAST:event_jBTNOverrideLateTimeActionPerformed

    private void jTBHighQualityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTBHighQualityActionPerformed
        network.setHighQuality(jTBHighQuality.isSelected());
        network.repaint();
    }//GEN-LAST:event_jTBHighQualityActionPerformed

    private void jBTOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTOpenActionPerformed
        openActionShowPrompt();
    }//GEN-LAST:event_jBTOpenActionPerformed

    private void jBTSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTSaveActionPerformed
        saveProject(false);
    }//GEN-LAST:event_jBTSaveActionPerformed

    private void jBTNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTNewActionPerformed
        newAction();
    }//GEN-LAST:event_jBTNewActionPerformed

    private void jBTEditActivityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTEditActivityActionPerformed
        Activity activity = (Activity) network.getUiSystem().getFocused();
        activity.showEditDialog();
    }//GEN-LAST:event_jBTEditActivityActionPerformed

    private void jBTDeleteActivityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTDeleteActivityActionPerformed
        Activity activity = (Activity) network.getUiSystem().getFocused();
        activity.showDeleteDialog();
    }//GEN-LAST:event_jBTDeleteActivityActionPerformed

    private void jTBLegendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTBLegendActionPerformed
        network.setLegend(jTBLegend.isSelected());
        network.repaint();
    }//GEN-LAST:event_jTBLegendActionPerformed

    private void jMIHelpContentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIHelpContentActionPerformed
        JDShowHTML.showHTML(Internationalization.getWordHelp(), "help", "help.html");
    }//GEN-LAST:event_jMIHelpContentActionPerformed

    private void jMIAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIAboutActionPerformed
        new JDAbout(null, true).setVisible(true);
    }//GEN-LAST:event_jMIAboutActionPerformed

    private void jBTNAddEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTNAddEventActionPerformed
        if (network.initialized) {
            if (network.hasSpace()) {
                AddEventCommand command = new AddEventCommand();
                Application.storeAndExecute(command);
            } else {
                JOptionPane.showMessageDialog(this, "Pert network is full !", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_jBTNAddEventActionPerformed

    private void jBTNCalculatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTNCalculatorActionPerformed
        (new JDCalculator(this, rootPaneCheckingEnabled)).setVisible(true);
    }//GEN-LAST:event_jBTNCalculatorActionPerformed

    private void jSPDecimalPlacesStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSPDecimalPlacesStateChanged
        network.setDecimalPlaces((Integer) jSPDecimalPlaces.getValue());
        network.repaint();
    }//GEN-LAST:event_jSPDecimalPlacesStateChanged

    private void jMIUndoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIUndoActionPerformed
        Application.undo();
        network.repaint();
    }//GEN-LAST:event_jMIUndoActionPerformed

    private void jMIRedoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIRedoActionPerformed
        Application.redo();
        network.repaint();
    }//GEN-LAST:event_jMIRedoActionPerformed

    private void jBTNUndoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTNUndoActionPerformed
        Application.undo();
        network.repaint();
    }//GEN-LAST:event_jBTNUndoActionPerformed

    private void jBTNRedoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTNRedoActionPerformed
        Application.redo();
        network.repaint();
    }//GEN-LAST:event_jBTNRedoActionPerformed

    private void evaluation() {
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().browse(new URI(Internationalization.getLinkEvaluation()));
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Internet conection error !", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void jMIEvaluateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIEvaluateActionPerformed
        evaluation();
    }//GEN-LAST:event_jMIEvaluateActionPerformed

    private void jBTNEvaluationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTNEvaluationActionPerformed
        evaluation();
    }//GEN-LAST:event_jBTNEvaluationActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // We have changes, we need to ask to save !
        if(Application.isUndoAvailable()){                        
            final int option = JOptionPane.showConfirmDialog(this, Internationalization.getConfirmFileSaveWhenClosing(), Internationalization.getWordQuestion(), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            switch(option){
                case JOptionPane.YES_OPTION:
                    saveProject(!jBTSave.isEnabled());
                    break;
                case JOptionPane.CANCEL_OPTION:
                    return;                    
            }
        }        
        System.exit(0);
    }//GEN-LAST:event_formWindowClosing

    private void jMIGanttActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIGanttActionPerformed
        exportTo(ExportFormat.GANTT_PNG);
    }//GEN-LAST:event_jMIGanttActionPerformed

    private void jTBConnectorLineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTBConnectorLineActionPerformed
        Constants.DEFAULT_CONNECTOR = ConnectorType.CONNECTOR_TYPE_LINE;
    }//GEN-LAST:event_jTBConnectorLineActionPerformed

    private void jTBConnectorCurveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTBConnectorCurveActionPerformed
        Constants.DEFAULT_CONNECTOR = ConnectorType.CONNECTOR_TYPE_CUBIC_BEZIER;
    }//GEN-LAST:event_jTBConnectorCurveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBTDeleteActivity;
    private javax.swing.JButton jBTEditActivity;
    private javax.swing.JButton jBTNAddEvent;
    private javax.swing.JButton jBTNCalculator;
    private javax.swing.JButton jBTNEvaluation;
    private javax.swing.JButton jBTNOverrideLateTime;
    private javax.swing.JButton jBTNRedo;
    private javax.swing.JButton jBTNUndo;
    private javax.swing.JButton jBTNew;
    private javax.swing.JButton jBTOpen;
    private javax.swing.JButton jBTSave;
    private javax.swing.JLabel jLBFontSize;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuItem jMIAbout;
    private javax.swing.JMenuItem jMIEvaluate;
    private javax.swing.JMenuItem jMIExit;
    private javax.swing.JMenuItem jMIExport;
    private javax.swing.JMenuItem jMIGantt;
    private javax.swing.JMenuItem jMIHelpContent;
    private javax.swing.JMenuItem jMINew;
    private javax.swing.JMenuItem jMIOpen;
    private javax.swing.JMenuItem jMIPrint;
    private javax.swing.JMenuItem jMIRedo;
    private javax.swing.JMenuItem jMISave;
    private javax.swing.JMenuItem jMISaveAs;
    private javax.swing.JMenuItem jMIUndo;
    private javax.swing.JMenu jMNEdit;
    private javax.swing.JMenu jMNExamples;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPNPreview;
    private javax.swing.JPanel jPanel0;
    private javax.swing.JSpinner jSPDecimalPlaces;
    private javax.swing.JSpinner jSPFontSize;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JToolBar.Separator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JToolBar.Separator jSeparator13;
    private javax.swing.JToolBar.Separator jSeparator14;
    private javax.swing.JToolBar.Separator jSeparator15;
    private javax.swing.JToolBar.Separator jSeparator16;
    private javax.swing.JToolBar.Separator jSeparator17;
    private javax.swing.JToolBar.Separator jSeparator18;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JToolBar.Separator jSeparator6;
    private javax.swing.JPopupMenu.Separator jSeparator7;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JToolBar.Separator jSeparator9;
    private javax.swing.JToggleButton jTBConnectorCurve;
    private javax.swing.JToggleButton jTBConnectorLine;
    private javax.swing.JToggleButton jTBHighQuality;
    private javax.swing.JToggleButton jTBLegend;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables
}
