/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.gui;

import br.com.joaodarcy.perttool.Activity;
import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.GraphCycleException;
import br.com.joaodarcy.perttool.Internationalization;
import br.com.joaodarcy.perttool.PertDocument;
import br.com.joaodarcy.perttool.ResourceManager;
import br.com.joaodarcy.perttool.cmd.EditActivityCommand;
import static br.com.joaodarcy.perttool.ui.DrawUtils.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;

public class JDManageActivity extends javax.swing.JDialog {

    private final PertDocument network;
    private final Activity activity;
    private final boolean editingNewActivity;
    
    private final KeyAdapter confirmWithEnterKey = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER){
                confirmAction();
            }
        }                
    };
       
    public JDManageActivity(PertDocument network, Activity activity, boolean editingNewActivity, java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        
        this.editingNewActivity = editingNewActivity;
        
        initComponents();
        
        this.setIconImage(ResourceManager.APP_LOGO);
        
        this.network = network;
        this.activity = activity;        
        this.setLocationRelativeTo(null);
        
        jTFWeight.setText(String.valueOf(activity.getWeight()));
        jTFDescription.setText(activity.getDescription());
        
        this.setTitle(String.format("%s - %s", Internationalization.getAppTitle(), Internationalization.getEditActivity()));
        
        switch(activity.getConnectorType()){
            case CONNECTOR_TYPE_LINE:
                jRBStraightLine.setSelected(true);
                break;
            case CONNECTOR_TYPE_CUBIC_BEZIER:
                jRBCubicBezierCurve.setSelected(true);
                break;
        }
        
        ButtonGroup connectorType = new ButtonGroup();
        connectorType.add(jRBStraightLine);
        connectorType.add(jRBCubicBezierCurve);
        
        jTFWeight.addKeyListener(confirmWithEnterKey);
        jTFDescription.addKeyListener(confirmWithEnterKey);
        
        jTFWeight.requestFocus();
        jTFWeight.setSelectionStart(0);
        jTFWeight.setSelectionEnd(jTFWeight.getText().length());
    }

    private void showError(String format, Object ... paramters){
        JOptionPane.showMessageDialog(this, String.format(format, paramters), this.getTitle(), JOptionPane.ERROR_MESSAGE);
    }
    
    private void confirmAction(){
        try{
            if(jTFWeight.getText().isEmpty()){
                throw new Exception("The weight should be informed!");
            }
            final double newWeight = Double.parseDouble(jTFWeight.getText().replace(',', '.'));
            final String newDescription = jTFDescription.getText();
            final ConnectorType newConnectorType;
            if(jRBStraightLine.isSelected()){
                newConnectorType = ConnectorType.CONNECTOR_TYPE_LINE;
            }else{
                newConnectorType = ConnectorType.CONNECTOR_TYPE_CUBIC_BEZIER;
            }
                
            EditActivityCommand command = new EditActivityCommand(activity, newDescription, newWeight, newConnectorType);
            if(editingNewActivity){
                activity.setWeight(newWeight);
                activity.setDescription(newDescription);
                
                // Detect and remove the Cycles b4 closing
                try{                
                    network.recalculateNetworkTime();                                
                }catch(GraphCycleException e){
                    showError("%s", e.getMessage());
                    network.delActivity(activity);
                    this.setVisible(false);
                    network.recalculateNetworkTime();
                }
            }else{
                Application.storeAndExecute(command);
            }                                                                    
            this.setVisible(false);
        }catch(Throwable e){
            showError("Invalid Weight: %s", e.getMessage());
            e.printStackTrace();
            return;
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTFWeight = new javax.swing.JTextField();
        jBTCancelar = new javax.swing.JButton();
        jBTOK = new javax.swing.JButton();
        jTFDescription = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jRBStraightLine = new javax.swing.JRadioButton();
        jRBCubicBezierCurve = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText(Internationalization.getWordWeight() + ":");

        jBTCancelar.setText(Internationalization.getWordCancel());
        jBTCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTCancelarActionPerformed(evt);
            }
        });

        jBTOK.setText(Internationalization.getWordOK());
        jBTOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBTOKActionPerformed(evt);
            }
        });

        jLabel4.setText(Internationalization.getWordDescription() + ":");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(Internationalization.getConnectorType()));

        jRBStraightLine.setMnemonic(KeyEvent.VK_L);
        jRBStraightLine.setText(Internationalization.getConnectorTypeLine());

        jRBCubicBezierCurve.setMnemonic(KeyEvent.VK_C);
        jRBCubicBezierCurve.setText(Internationalization.getConnectorTypeCurve());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jRBStraightLine)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jRBCubicBezierCurve)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRBStraightLine)
                    .addComponent(jRBCubicBezierCurve))
                .addGap(0, 5, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 106, Short.MAX_VALUE)
                                .addComponent(jBTOK)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBTCancelar))
                            .addComponent(jTFDescription)
                            .addComponent(jTFWeight)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTFWeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBTOK)
                    .addComponent(jBTCancelar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBTCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTCancelarActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_jBTCancelarActionPerformed

    private void jBTOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBTOKActionPerformed
        confirmAction();
    }//GEN-LAST:event_jBTOKActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBTCancelar;
    private javax.swing.JButton jBTOK;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRBCubicBezierCurve;
    private javax.swing.JRadioButton jRBStraightLine;
    private javax.swing.JTextField jTFDescription;
    private javax.swing.JTextField jTFWeight;
    // End of variables declaration//GEN-END:variables
}
