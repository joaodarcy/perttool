/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import br.com.joaodarcy.perttool.desenhar.Constants;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import br.com.joaodarcy.perttool.ui.Rectangle;
import br.com.joaodarcy.perttool.ui.UIElement;
import br.com.joaodarcy.perttool.ui.UISystem;
import br.com.joaodarcy.perttool.math.Vector2i;
import static br.com.joaodarcy.perttool.desenhar.Constants.*;
import br.com.joaodarcy.perttool.gui.ExportFormat;
import br.com.joaodarcy.perttool.math.Vector2f;
import br.com.joaodarcy.perttool.ui.DrawUtils;
import java.awt.Font;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

/**
 * Classe que gera a imagem da Rede PERT, pode ser utilizado para exportar a 
 * imagem da rede para JPEG ou PNG se necessário.
 */
public final class PertImageGenerator {
           
    private int fontSize;
    private Font font;
    private boolean highQuality;
    private boolean showLegend;
    
    public final static int LOGO_WIDTH = 140;
    public final static int LOGO_HEIGHT = 58;
    
    public PertImageGenerator() {
        this.fontSize = 12;
        this.highQuality = true;
        this.showLegend = true;
    }   

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
        this.font = null;
    }                      

    public void setShowLegend(boolean showLegend) {
        this.showLegend = showLegend;
    }

    public boolean isShowLegend() {
        return showLegend;
    }        
    
    public boolean exportToImage(File fileOut, PertDocument doc, ExportFormat format){
        BufferedImage image = new BufferedImage(doc.getWidth(), doc.getHeight(), BufferedImage.TYPE_INT_ARGB);
        
        Graphics2D g = image.createGraphics();
        if(format == ExportFormat.PERT_PNG){                
            drawDiagramFor(g, doc, image.getWidth(), image.getHeight(), false);        
        }else{           
            GanttGenerator gen = new GanttGenerator();
            gen.draw(g, image.getWidth());
            
            // Write the Logo
            drawLogo(g, image.getWidth(), image.getHeight());
        }
        g.dispose();
        
        try{
            ImageIO.write(image, "PNG", fileOut);
            return true;
        }catch(Throwable t){
            t.printStackTrace();
        }
        return false;
    }
        
    private void drawLegend(Graphics2D g, PertDocument doc, boolean screenDraw){
        if(!showLegend /*|| !screenDraw  Closes #20*/){
            return;
        }        
        
        if(fontSize > 24){
            g.setFont(g.getFont().deriveFont(24.0f));
        }
        final FontMetrics metrics = g.getFontMetrics();
        
        float scaleFactorX = fontSize < 24 ? (float)fontSize / 12.0f : 2.0f;
        float scaleFactorY = fontSize < 22 ? 1.0f : Math.min((float)fontSize, 24.0f) / 22.0f;
        
        int legendWidth = (int)(228.0f * scaleFactorX), legendHeight = (int)(110.0f * scaleFactorY);
        int padding = 8;
        int drawX = doc.getWidth() - legendWidth - padding;
        int drawY = padding;
        
        int textDrawY = (drawY + 14) + metrics.getHeight()/ 2;
        int textDrawX = drawX + 17 + 16 + padding;
        int textDrawYInc = 20;
        int imgDrawX = drawX + 10;
        
        //g.setFont(g.getFont().deriveFont(12.0f));
        
        g.setStroke(Constants.ARROW_LINE_PHANTHOM_STROKE);
        
        //Color fillColor = new Color(1.0f, 1.0f, 1.0f, 0.9f);        
        //g.setColor(fillColor);
        g.setColor(Color.WHITE);
        g.fillRect(drawX, drawY, legendWidth, legendHeight);
        
        g.setColor(Color.BLACK);
        g.drawRect(drawX, drawY, legendWidth, legendHeight);
        
        // Initial Event
        g.drawImage(ResourceManager.EVENT_START_IMAGE, imgDrawX, drawY + 10, 16, 16, null);
        g.drawString(Internationalization.getStartEvent(), textDrawX, textDrawY);
        
        // Final Event
        g.drawImage(ResourceManager.EVENT_END_IMAGE, imgDrawX, drawY + 29, 16, 16, null);
        g.drawString(Internationalization.getEndEvent(), textDrawX, textDrawY + textDrawYInc);
        
        // Critical Path
        g.setStroke(Constants.ARROW_LINE_STROKE);
        g.setColor(Color.ORANGE);
        g.drawLine(imgDrawX, drawY + 48 + 8, imgDrawX + 16, drawY + 48 + 8);
        g.setColor(Color.BLACK);
        g.drawString(Internationalization.getCriticalPath(), textDrawX, textDrawY + textDrawYInc * 2);
        
        // Phantom
        g.setStroke(Constants.ARROW_LINE_PHANTHOM_STROKE);
        g.setColor(Color.LIGHT_GRAY);
        g.drawLine(imgDrawX, drawY + 67 + 8, imgDrawX + 16, drawY + 67 + 8);
        g.setColor(Color.BLACK);
        g.drawString(Internationalization.getPhantomActivity(), textDrawX, textDrawY + textDrawYInc * 3);
        
        // Early Time / ... / Gap
        String timeString = String.format("0/0/0 %s/%s/%s", Internationalization.getEarlyTime(), Internationalization.getLateTime(), Internationalization.getWordGap());
        g.drawString(timeString, imgDrawX, (drawY + 94) + metrics.getHeight()/ 2);
    }
    
    private void drawActivitesFor(Graphics2D g, PertDocument doc, boolean screenDraw){
        final Stroke oldStroke = g.getStroke();
        final FontMetrics metrics = g.getFontMetrics();
             
        // Draw lines from the center of an event to another
        for(Activity a : doc.activitiesList){
            // Catching the start point            
            final Vector2f srcPoint, dstPoint;
            final Vector2f srcCenter, dstCenter;
            final Event source, destination;
            {
                source = doc.eventMap.get(a.eventStart);
                destination = doc.eventMap.get(a.eventEnd);
                
                srcCenter = Rectangle.getCenterEx(source);
                dstCenter = Rectangle.getCenterEx(destination);
                
                srcPoint = new Vector2f(srcCenter);
                dstPoint = new Vector2f(dstCenter);
            }                                    
                            
            //
            // If is phatom change the trace
            Stroke stroke = a.isPhatom() ? ARROW_LINE_PHANTHOM_STROKE : ARROW_LINE_STROKE;
                           
            // We put the line color and draw a straight line from one center to another          
            if(a.isCriticalPath()){
                g.setColor(Color.ORANGE);
            }else{
                g.setColor(Color.LIGHT_GRAY);
            }
            
            final Vector2f anglePoint = DrawUtils.drawConnector(g, stroke, srcPoint, dstPoint, a.getConnectorType(), false);
            
            /** Line Angle */
            double angle;
            
            AffineTransform old = g.getTransform();
            
            // Draw arrow now because we dont want it over the text
            {
                // Calculations the length, angle and straight line distance to position
                // the arrow correctly.
                double dx = dstPoint.x - anglePoint.x, dy = dstPoint.y - anglePoint.y;
                angle = Math.atan2(dy, dx);
                
                Vector2f drawPoint = new Vector2f(
                    (dstCenter.x - ((float)Math.cos(angle) * (Constants.ARROW_IMAGE_HALF_WIDTH + Constants.EVT_HALF_RADIUS_FLOAT))) - Constants.ARROW_IMAGE_HALF_WIDTH,
                    (dstCenter.y - ((float)Math.sin(angle) * (Constants.ARROW_IMAGE_HALF_HEIGTH + Constants.EVT_HALF_RADIUS_FLOAT))) - Constants.ARROW_IMAGE_HALF_HEIGTH
                );
                
                AffineTransform transform = AffineTransform.getRotateInstance(angle, Constants.ARROW_IMAGE_HALF_WIDTH, Constants.ARROW_IMAGE_HALF_HEIGTH);
                AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
                
                // Draw the arrow
                g.drawImage(op.filter(ResourceManager.ARROW_IMAGE, null), (int)drawPoint.x, (int)drawPoint.y, (int)Constants.ARROW_IMAGE_WIDTH, (int)Constants.ARROW_IMAGE_HEIGTH, null);
                
                g.setTransform(new AffineTransform());
            }
            
            // We convert the weight to String                     
            String strPeso = a.isPhatom() ? "0" : String.format(doc.getAcitivyWeightFormatString(), a.getWeight());            
                        
            int lineCenterX = (int)((dstPoint.x + srcPoint.x) / 2.0f);
            int lineCenterY = (int)((dstPoint.y + srcPoint.y) / 2.0f);
            
            int textX = lineCenterX;
            int textY = lineCenterY;                                              
            
            // We put the color that we will use in the text weights
            g.setColor(Color.BLACK);           
                        
            AffineTransform transformText = AffineTransform.getTranslateInstance(textX, textY);
            transformText.concatenate(AffineTransform.getRotateInstance(angle));
            
            g.setTransform(transformText); 
                  
            if(!a.isPhatom()){
                int weightTextWidth = metrics.stringWidth(strPeso);            
                g.drawString(strPeso, - (weightTextWidth / 2), metrics.getDescent() * 6);
            }
            
            if(a.getDescription() != null && !a.getDescription().isEmpty()){
                //g.setColor(Color.BLACK);
                int descTextWidth = metrics.stringWidth(a.getDescription());
                g.drawString(a.getDescription(), - (descTextWidth / 2), -(metrics.getDescent() * 4));            
            }
            g.setTransform(old);

            if(screenDraw){
                g.setColor(Color.LIGHT_GRAY);
                g.fillOval(lineCenterX - 3, lineCenterY - 3, 6, 6);
            }
            
            // Set the position for the Activity in view so we can click on it.
            a.setPosition(lineCenterX - 8, lineCenterY - 8);
            a.setSize(16, 16);
        }
        
        g.setStroke(oldStroke);
    }                

    public void setHighQuality(boolean hightQuality) {
        this.highQuality = hightQuality;
    }        
            
    public void drawDiagramFor(Graphics g, PertDocument doc, int width, int height) {
        this.drawDiagramFor(g, doc, width, height, true);
    }
         
    private void drawLogo(Graphics g, PertDocument doc){
        drawLogo(g, doc.getWidth(), doc.getHeight());
    }
    
    private void drawLogo(Graphics g, int width, int height){
        // Write the Logo
        g.drawImage(
            ResourceManager.LOGO, 
            width - LOGO_WIDTH - 8, 
            height - LOGO_HEIGHT - 58 - 8, 
            LOGO_WIDTH, 
            LOGO_HEIGHT, 
            null
        );
    }
    
    public static void enableHighQualityFor(Graphics2D g){
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        rh.add(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        rh.add(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
        rh.add(new RenderingHints(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY));

        g.setRenderingHints(rh);
    }
    
    public void drawDiagramFor(Graphics g1d, PertDocument doc, int width, int height, boolean screenDraw) {
        Graphics2D g = (Graphics2D)g1d;
        
        // Always use HighQuality when Drawing to image output !
        if(highQuality || !screenDraw){
            enableHighQualityFor(g);     
        }
        
        // Recreate the font if needed
        if(font == null){
            font = new Font("Arial", Font.BOLD, fontSize);            
        }
        g.setFont(font);
        
        // Clean the image
        if(screenDraw){
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, width, height);
        }
        
        // Write the Logo
        drawLogo(g, doc);
        
        // Draw the activities (Arrows and Weight)        
        drawActivitesFor(g, doc, screenDraw);
                
        doc.getUiSystem().render(g);        
        
        if(screenDraw){                            
            final UISystem uiSystem = doc.getUiSystem();
            final UIElement focused = uiSystem.getFocused();

            if(focused != uiSystem.getDesktop()){
                final UIElement selected;
                if(Event.isEvent(focused) || NewActivityButton.isNewActivityButton(focused)){
                    selected = focused.getParent();                
                }else{
                    selected = focused;
                }                        
                final Vector2i position = selected.getViewPosition();
                final Vector2i size = selected.getSize();

                g.setColor(Color.GRAY);
                g.setStroke(Constants.ARROW_LINE_PHANTHOM_STROKE);                
                g.drawRect(position.getX() - 4, position.getY() - 4, size.getX() + 8, size.getY() + 8);
            }
        }
        
        drawLegend(g, doc, screenDraw);
    }        
}
