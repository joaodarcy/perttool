/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public final class Internationalization {
   
    private final static String MESSAGE_FILE_PATH;        
    private final static Properties exampleFiles;
    private static ResourceBundle messages;    
    private static String languageDir;
    
    static{
        MESSAGE_FILE_PATH = "br.com.joaodarcy.perttool.MessagesBundle";
        languageDir = "en";
        messages = loadMessageFile();
        exampleFiles = new Properties();        
        try{
            exampleFiles.load(Internationalization.class.getClassLoader().getResourceAsStream("example/example_files.properties"));
        }catch(Throwable t){
            t.printStackTrace();
        }
    }

    private Internationalization() {
        throw new AssertionError();
    }

    public static String getLanguageDir() {
        return languageDir;
    }        
    
    public static void changeLocale(Locale locale){
        Locale.setDefault(locale);
        messages = loadMessageFile(locale);
    }
    
    private static ResourceBundle loadMessageFile(){
        languageDir = Locale.getDefault().getLanguage();
        return ResourceBundle.getBundle(MESSAGE_FILE_PATH);
    }
    
    private static ResourceBundle loadMessageFile(Locale locale){
        languageDir = locale.getLanguage();
        return ResourceBundle.getBundle(MESSAGE_FILE_PATH, locale);
    }
    
    public static String getExampleFileName(Object entryKey){
        String key = exampleFiles.getProperty((String)entryKey, entryKey.toString());
        return messages.getString(key);
    }
    
    public static Set<Object> getExampleFileList(){
        return exampleFiles.keySet();
    }
    
    public static String getHTMLFile(String folder, String fileName){
        final String htmlFile = String.format("./%s/%s/%s", folder, languageDir, fileName);
        
        try{            
            byte[] rawData = Files.readAllBytes(Paths.get(htmlFile));
            return new String(rawData, "UTF-8");
        }catch(Throwable t){
            t.printStackTrace();
        }        
        return "<html><h1>HTML File Not Found: " + htmlFile + "</h1></html>";
    }
             
    public static String getConfirmFileSaveWhenClosing(){
        return messages.getString("confirm_file_save_when_closing");
    }      
    
    public static String getInitialEventCantBeDelete(){
        return messages.getString("initial_event_cant_be_deleted");
    }
    
    public static String getFinalEventCantBeDelete(){
        return messages.getString("final_event_cant_be_deleted");
    }

    public static String getInitialEventCantHaveReferencingActivities(){
        return messages.getString("initial_event_cant_have_referencing_activities");
    }
    
    public static String getFinalEventCantHaveChildActivites(){
        return messages.getString("final_event_cant_have_child_activities");
    }
    
    public static String getPNGExport(){        
        return messages.getString("png_export");
    }
    
    public static String getGanttExport(){
        return messages.getString("gantt_export");
    }
    
    public static String getDecimalPlaces(){
        return messages.getString("decimal_places");
    }
    
    public static String getMakeFinalEvent(){
        return messages.getString("make_final_event");
    }
    
    public static String getMakeInitialEvent(){
        return messages.getString("make_initial_event");
    }
    
    public static String getMessage(String key){
        return messages.getString(key);
    }
    
    public static String getWordExample(){
        return messages.getString("word.example");
    }
    
    public static String getSetupNetwork(){
        return messages.getString("setup_network");
    }
    
    public static String getRestartNetwork(){
        return messages.getString("restart_network");
    }
    
    public static String getNewActivity(){
        return messages.getString("new_activity");
    }
    
    public static String getCalculateExecutionTime(){
        return messages.getString("calc_execution_time");
    }
    
    public static String getPrintNetwork(){
        return messages.getString("print_network");
    }
    
    public static String getWordActivity(){
        return messages.getString("word.activity");
    }
    
    public static String getWordPreview(){
        return messages.getString("word.preview");
    }
        
    public static String getWordOK(){
        return messages.getString("word.ok");
    }
    
    public static String getWordCancel(){
        return messages.getString("word.cancel");
    }
    
    public static String getAppTitle(){
        return messages.getString("app_title");
    }    
    
    public static String getEventNum(){
        return messages.getString("event_num");
    }
    
    public static String getStartEvent(){
        return messages.getString("start_event");
    }
    
    public static String getEndEvent(){
        return messages.getString("end_event");
    }
    
    public static String getWordEvent(){
        return messages.getString("word.event");
    }
    
    public static String getWordWeight(){
        return messages.getString("word.weight");
    }
    
    public static String getEditActivity(){
        return messages.getString("edit_activity");
    }
    
    public static String getCalculatorActivity(){
        return messages.getString("calculator_activity");
    }

    public static String getFontSize(){
        return messages.getString("font_size");
    }    
    
    public static String getWordPhanthom(){
        return messages.getString("word.phantom");
    }
    
    public static String getConfirmDeleteActivityMessage(){
        return messages.getString("confirm_delete_activity_msg");
    }
    
    public static String getConfirmDeleteActivityTitle(){
        return messages.getString("confirm_delete_activity_title");
    }
    
    public static String getWordFile(){
        return messages.getString("word.file");
    }
    
    public static String getWordNew(){
        return messages.getString("word.new");
    }
    
    public static String getWordOpen(){
        return messages.getString("word.open");
    }
    
    public static String getWordSave(){
        return messages.getString("word.save");
    }
    
    public static String getWordSaveAs(){
        return messages.getString("word.saveAs");
    }
    
    public static String getWordPrint(){
        return messages.getString("word.print");
    }
    
    public static String getWordExport(){
        return messages.getString("word.export");
    }
    
    public static String getWordHelp(){
        return messages.getString("word.help");
    }
    
    public static String getWordAbout(){
        return messages.getString("word.about");
    }
    
    public static String getWordExit(){
        return messages.getString("word.exit");
    }
    
    public static String getWordEdit(){
        return messages.getString("word.edit");
    }
    
    public static String getWordUndo(){
        return messages.getString("word.undo");
    }
    
    public static String getWordRedo(){
        return messages.getString("word.redo");
    }
    
    public static String getExportImgSuccess(){
        return messages.getString("export_img_success");
    }
    
    public static String getExportImgError(){
        return messages.getString("export_img_error");
    }
    
    public static String getSaveSuccess(){
        return messages.getString("save_success");
    }
    
    public static String getSaveError(){
        return messages.getString("save_error");
    }
    
    public static String getOpenError(){
        return messages.getString("open_error");
    }
    
    public static String getSaveErrorDiagramEmpty(){
        return messages.getString("save_error_diagram_empty");
    }
    
    public static String getInitialEventLegend(){
        return messages.getString("initial_event_legend");
    }

    public static String getFinalEventLegend(){
        return messages.getString("final_event_legend");
    }
    
    public static String getHighQuality(){
        return messages.getString("high_quality");
    }
    
    public static String getUntitled(){
        return messages.getString("untitled");
    }
    
    public static String getOverrideLateTime(){
        return messages.getString("override_late_time");
    }
    
    public static String getEarlyTime(){
        return messages.getString("early_time");
    }
    
    public static String getWordGap(){
        return messages.getString("word.gap");
    }
    
    public static String getLateTime(){
        return messages.getString("late_time");
    }
    
    public static String getWordTip(){
        return messages.getString("word.tip");
    }
    
    public static String getWordClose(){
        return messages.getString("word.close");
    }
    
    public static String getTipChangeLateTime(){
        return messages.getString("tip_change_late_time");
    }
    
    public static String getAboutProject(){
        return messages.getString("about_project");
    }
    
    public static String getTextAboutProject(){
        return messages.getString("text_about_project");
    }
    
    public static String getAboutDevelopers(){
        return messages.getString("about_developers");
    }
    
    public static String getTextAboutDevelopers(){
        return messages.getString("text_about_developers");
    }
    
    public static String getErrorMustBeGreaterThan(String field, int value){
        return String.format(
            messages.getString("error_value_must_be_greater"), 
            field, 
            value
        );
    }
    
    public static String getErrorMustBeLowerThan(String field, int value){
        return String.format(
            messages.getString("error_value_must_be_lower"), 
            field, 
            value
        );
    }
    
    public static String getCycleFoundError(){
        return messages.getString("cycle_found_error");
    }
    
    public static String getWordDescription(){
        return messages.getString("word.description");
    }
    
    public static String getCriticalPath(){
        return messages.getString("critical_path");        
    }
    
    public static String getPhantomActivity(){
        return messages.getString("phantom_activity");
    }    
    
    public static String getWordLegend(){
        return messages.getString("word.legend");
    }
    
    public static String getWordLanguage(){
        return messages.getString("word.language");
    }

    public static String getWordEnglish(){
        return messages.getString("word.english");
    }

    public static String getWordSpanish(){
        return messages.getString("word.spanish");
    }

    public static String getWordPortuguese(){
        return messages.getString("word.portuguese");
    }
    
    public static String getAddNewEvent(){
        return messages.getString("add_new_event");
    }
    
    public static String getMsgConfirmDeleteEvent(){
        return messages.getString("confirm_delete_event_msg");
    }
    
    public static String getDeleteEvent(){
        return messages.getString("delete_event");
    }

    public static String getOptimisticTime(){
        return messages.getString("optimistic_time");
    }

    public static String getNormalTime(){
        return messages.getString("normal_time");
    }

    public static String getPessimisticTime(){
        return messages.getString("pessimistic_time");
    }

    public static String getExpectedTime(){
        return messages.getString("expected_time");
    }
    
    public static String getDefaultExpectedTimeMsg() {
        return messages.getString("default_expected_time_msg");
    }

    public static String getWordEvaluation() {
        return messages.getString("word.evaluation");
    }
    
    public static String getWordQuestion(){
        return messages.getString("word.question");
    }

    public static String getLinkEvaluation(){
        return messages.getString("link_evaluation");
    }

    public static String getConnectorType(){
        return messages.getString("connector_type");
    }
    
    public static String getConnectorTypeLine(){
        return messages.getString("connector_type_line");
    }
    
    public static String getConnectorTypeCurve(){
        return messages.getString("connector_type_curve");
    }
}
