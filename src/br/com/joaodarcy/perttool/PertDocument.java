/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import br.com.joaodarcy.perttool.cmd.DeleteActivityPreventingUpdateCommand;
import br.com.joaodarcy.perttool.cmd.DeleteEventCommand;
import br.com.joaodarcy.perttool.cmd.MacroCommand;
import br.com.joaodarcy.perttool.cmd.RefreshNetworkCommand;
import br.com.joaodarcy.perttool.desenhar.Constants;
import br.com.joaodarcy.perttool.gui.ExportFormat;
import br.com.joaodarcy.perttool.gui.JDManageActivity;
import static br.com.joaodarcy.perttool.ui.DrawUtils.*;
import br.com.joaodarcy.perttool.ui.UIElement;
import br.com.joaodarcy.perttool.ui.UIListener;
import br.com.joaodarcy.perttool.ui.UISystem;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The PERT Network
 */
public class PertDocument extends JPanel {
    
    private final static int BACKBUFFER_WIDTH = 1920;
    private final static int BACKBUFFER_HEIGHT = 1080;
    
    public final static int MIN_DECIMAL_PLACES = 0;
    public final static int MAX_DECIMAL_PLACES = 6;
    
    public final static int MIN_EVENT_COUNT = 2;
    public final static int MAX_EVENT_COUNT = 512;
    
    protected final static String MSG_UNCONFIGURED_NETWORK = "The network has not been set!";
    
    public boolean initialized;    
    
    public Integer eventStart;
    public Integer eventEnd;
    
    public Map<Integer, Event> eventMap;
    public List<Activity> activitiesList;    
    
    protected CriticalPathSearch criticalPathSearch;
    protected PertImageGenerator imageGenerator;
    protected UISystem uiSystem;
    
    protected int width;
    protected int height;
                       
    private int decimalPlaces;
    
    private String acitivyWeightFormatString;
    private String eventWeightFormatString;    
    
    boolean imageExporting;
    private boolean overrideLateTime;
    private double overrideLateTimeValue;
    
    private Image drawCache;
    
    public PertDocument() {
        this(300, 300);
    }
        
    public PertDocument(int width, int height) {
        this.width = width;
        this.height = height;
        this.eventMap = new HashMap<>();
        this.activitiesList = new ArrayList<>();
        this.initialized = false;
        this.uiSystem = new UISystem(this, BACKBUFFER_WIDTH, BACKBUFFER_HEIGHT);
        this.imageGenerator = new PertImageGenerator();
        this.imageExporting = false;
        this.criticalPathSearch = new DAGCriticalPathSearch();
        this.eventWeightFormatString = this.acitivyWeightFormatString = null;
        this.setDecimalPlaces(MIN_DECIMAL_PLACES);
        this.drawCache = new BufferedImage(BACKBUFFER_WIDTH, BACKBUFFER_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        
        this.addMouseMotionListener(uiSystem);
        this.addMouseListener(uiSystem);
        this.addKeyListener(uiSystem);
        
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                // FIXME: Not the right place for this code !
                //UIElement desktop = uiSystem.getDesktop();
                //desktop.setSize(1920, 1080);
                
                // When downscaling we dont lose components :D
                //for(UIElement child : desktop.getChildList()){
                    //child.moveAmount(0, 0, true);
                //}                
                repaint();
            }
        });
    }
    
    public String getAcitivyWeightFormatString() {
        return acitivyWeightFormatString;
    }

    public String getEventWeightFormatString() {
        return eventWeightFormatString;
    }
            
    public final void setDecimalPlaces(int decimalPlaces) {        
        if(decimalPlaces < MIN_DECIMAL_PLACES){
            decimalPlaces = 0;
        }else if(decimalPlaces > MAX_DECIMAL_PLACES){
            decimalPlaces = MAX_DECIMAL_PLACES;
        }
        this.decimalPlaces = decimalPlaces;
        this.acitivyWeightFormatString = String.format("%%.%df", decimalPlaces);
        this.eventWeightFormatString = String.format("%s/%s/%s", this.acitivyWeightFormatString, this.acitivyWeightFormatString, this.acitivyWeightFormatString);
    }

    public int getDecimalPlaces() {
        return decimalPlaces;
    }            

    public boolean hasSpace(){
        return eventMap.size() < MAX_EVENT_COUNT;
    }
    
    public void deleteEventById(int eventId){
        if(eventId == MAX_EVENT_COUNT){
            return;
        }        
        try{
            Event evt = eventMap.get(eventId);
            if(evt != null){
                deleteEvent(evt);
            }
        }catch(Throwable t){
            t.printStackTrace();
        }
    }
    
    public void deleteEvent(Event e) throws PertException{
        if(e == null){
            return;
        }
        switch(e.getType()){
            case EVENT_TYPE_START:
                throw new PertException(Internationalization.getInitialEventCantBeDelete());                
            case EVENT_TYPE_END:
                throw new PertException(Internationalization.getFinalEventCantBeDelete());                
        }
        
        MacroCommand command = new MacroCommand();
        // First command: To refresh when unExecuting
        command.add(new RefreshNetworkCommand(false, true));
        
        
        List<Activity> referenceList = getActivitiesThatReferences(e);
        for(Activity a : referenceList){
            command.add(new DeleteActivityPreventingUpdateCommand(a));
        }
        
        command.add(new DeleteEventCommand(e));
                        
        // Last command: To refresh when executing
        command.add(new RefreshNetworkCommand(true, false));
        
        Application.storeAndExecute(command);
    }
    
    public void setLegend(boolean showLegend){
        imageGenerator.setShowLegend(showLegend);
    }
    
    public void addUIListener(UIListener listener){
        uiSystem.addUIListener(listener);
    }
    
    public void cleanCriticalPathFlag(){
        for(Activity activity : activitiesList){
            activity.setCriticalPath(false);
        }
    }
    
    public int getActivityCount(){
        return activitiesList.size();
    }
    
    public int getEventCount(){
        return eventMap.size();
    }
    
    public Event getEventById(int eventId){
        return eventMap.get(eventId);
    }
    
    public int getFinalEventId(){
        return eventEnd;
    }
    
    public Event getFinalEvent(){
        if(initialized){
            return eventMap.get(eventEnd);
        }
        return null;
    }
    
    public void setOverrideLateTimeValue(double overrideLateTimeValue) {       
        this.overrideLateTimeValue = overrideLateTimeValue;
    }   
    
    public double getOverrideLateTimeValue() {
        return overrideLateTimeValue;
    }        
    
    public void setOverrideLateTime(boolean overrideLateTime) {
        this.overrideLateTime = overrideLateTime;        
    }   

    public boolean isOverrideLateTime() {
        return overrideLateTime;
    }        
    
    public void setHighQuality(boolean enableAA){
        imageGenerator.setHighQuality(enableAA);
    }
    
    public void setFontSize(int fontSize) {
        imageGenerator.setFontSize(fontSize);
    }   
    
    public UISystem getUiSystem() {
        return uiSystem;
    }        
    
    public Activity getActivityByStartAndEndEvent(int eventStart, int eventEnd){
        for(Activity a : activitiesList){
            if(a.eventStart == eventStart && a.eventEnd == eventEnd){
                return a;
            }
        }
        return null;
    }
    
    /**
     * Get all activities that starts at a give event. ( Adjacent ones )
     * 
     * @param eventId
     * @return 
     */
    public List<Activity> getStartingActivitesInEvent(int eventId){
        List<Activity> startingList = new ArrayList<>(activitiesList.size());
        for(Activity a : activitiesList){
            if(a.eventStart == eventId){
                startingList.add(a);
            }
        }
        return startingList;
    }
    
    /**
     * Adjacent ones.
     * 
     * @param eventId
     * @return 
     */
    public List<Activity> getActivitiesWhichTheEventDepends(int eventId){
        List<Activity> dependenciaList = new ArrayList<>(activitiesList.size());
        
        for(Activity a : activitiesList){
            if(a.eventEnd == eventId){
                dependenciaList.add(a);
            }
        }
        
        return dependenciaList;
    }    
    
    public List<Activity> getActivitiesThatReferences(Event e){
        List<Activity> referenceList = new ArrayList<>(activitiesList.size());        
        for(Activity a : activitiesList){
            if(a.eventEnd == e.id || a.eventStart == e.id){
                referenceList.add(a);
            }
        }        
        return referenceList;
    }
    
    /**
     * Catch all activities which the Event X depends and put in a list, utilized mainly for the runtime calculation.
     */    
    public List<Activity> getActivitiesWhichTheEventDepends(Event e){
        List<Activity> dependenciaList = new ArrayList<>(activitiesList.size());
        
        for(Activity a : activitiesList){
            if(a.eventEnd == e.id){
                dependenciaList.add(a);
            }
        }
        
        return dependenciaList;
    }
    
    /**
     * Calculates recursively event time.
     */
    protected void calculateEarlyStart(Event e, final Map<Integer, Boolean> calculatedFlagMap) {        
        if(calculatedFlagMap.get(e.id) == Boolean.TRUE){
            return;
        }
        
        List<Activity> dependenciaList = getActivitiesWhichTheEventDepends(e);
        
        double tempoCedo = 0.0;
        boolean tempoCedoCalculado = false;
        
        for(Activity a : dependenciaList){
            Event eventoAnterior = eventMap.get(a.eventStart);
            if(calculatedFlagMap.get(eventoAnterior.id) == Boolean.FALSE){
                calculateEarlyStart(eventoAnterior, calculatedFlagMap);
            }
                       
            if(tempoCedoCalculado){
                double tempoCedoTmp = eventoAnterior.getEarlyTime() + a.getWeight();
                if(tempoCedoTmp > tempoCedo){
                    tempoCedo = tempoCedoTmp;
                }
            }else{
                tempoCedo = eventoAnterior.getEarlyTime() + a.getWeight();
                tempoCedoCalculado = true;
            }
        }       
        
        e.setEarlyTime(tempoCedo);
        calculatedFlagMap.put(e.id, Boolean.TRUE);
    }
    
    protected void calculateLateStart(Event e, final Map<Integer, Boolean> calculatedFlagMap) {                
        List<Activity> dependenciaList = getActivitiesWhichTheEventDepends(e);
                
        if(e.getType() == EventType.EVENT_TYPE_END){
            if(overrideLateTime){
                e.setLateStart(overrideLateTimeValue);
            }else{
                e.setLateStart(e.getEarlyTime());
            }
            e.setGap(e.getLateStart() - e.getEarlyTime());
        }
        
        for(Activity a : dependenciaList){
            Event eventoAnterior = eventMap.get(a.eventStart);
            double tempoTardeTmp = e.getLateStart() - a.getWeight();
            if(calculatedFlagMap.get(eventoAnterior.id) == Boolean.FALSE){
                eventoAnterior.setLateStart(tempoTardeTmp);
                eventoAnterior.setGap(tempoTardeTmp - eventoAnterior.getEarlyTime());
                calculatedFlagMap.put(eventoAnterior.id, Boolean.TRUE);
            }else{
                if(eventoAnterior.getLateStart() > tempoTardeTmp){
                    eventoAnterior.setLateStart(tempoTardeTmp);
                    eventoAnterior.setGap(tempoTardeTmp - eventoAnterior.getEarlyTime());
                }
            }            
        }
        
        for(Activity a : dependenciaList){
            Event eventoAnterior = eventMap.get(a.eventStart);
            calculateLateStart(eventoAnterior, calculatedFlagMap);
        }
    }
    
    /**
     * Calculates the runtime of network
     * 
     * @throws br.com.joaodarcy.perttool.GraphCycleException When i cycle is found
     */
    public void recalculateNetworkTime() throws GraphCycleException {
        Map<Integer, Boolean> calculatedFlagMap = new HashMap<>(getEventCount());
        for(Event e : eventMap.values()){
            calculatedFlagMap.put(e.id, Boolean.FALSE);
            e.setEarlyTime(0.0);
            e.setLateStart(0.0);
            e.setGap(0.0);
        }
        try{
            for(Event e : eventMap.values()){            
                calculateEarlyStart(e, calculatedFlagMap);
            }
        }catch(StackOverflowError e){            
            e.printStackTrace();
            throw new GraphCycleException(Internationalization.getCycleFoundError(), e);
        }
        
        for(Event e : eventMap.values()){
            calculatedFlagMap.put(e.id, Boolean.FALSE);
        }
        calculateLateStart(eventMap.get(eventEnd), calculatedFlagMap);
        
        cleanCriticalPathFlag();
        criticalPathSearch.findCriticalPath(this);
    }
            
    public void addActivity(Activity activity) throws PertException{
        addActivity(activity, false);
    }
    
    public void addActivity(Activity activity, boolean preventUpdate) throws PertException, GraphCycleException {
        if(!initialized){
            throw new AssertionError(MSG_UNCONFIGURED_NETWORK);
        }
        if(activity == null){
            throw new AssertionError("Null Activity !");
        }
        if(activity.eventStart == activity.eventEnd){
            throw new AssertionError("The Start Event must be different of Event End.");
        }
        if(activity.eventEnd == eventStart){
            throw new AssertionError("You can't add an activity in the start event !");
        }
        
        List<Activity> myActivitiesList = getStartingActivitesInEvent(activity.eventStart);
        for(Activity existingActivity : myActivitiesList){
            if(existingActivity.eventEnd == activity.eventEnd){
                throw new AssertionError("You already have an Activity to this destination !");
            }
        }        
        
        activitiesList.add(activity);
        uiSystem.getDesktop().addChild(activity);
        
        if(!preventUpdate){
            // 
            // Please be advised that we change the list of activities to which the network can be recalculated.
            recalculateNetworkTime();
        }
    }
    
     public void delActivity(Activity activity) {
         delActivity(activity, false);
     }
    
    public void delActivity(Activity activity, boolean preventUpdate) {
        if(activity == null){
            return;
        }
        if(activitiesList.remove(activity)){
            uiSystem.setFocus(uiSystem.getDesktop());
            uiSystem.getDesktop().removeChild(activity);
            if(preventUpdate == false){
                try{
                    recalculateNetworkTime();
                }catch(Throwable t){
                    t.printStackTrace();
                }
            }
        }
    }
    
    /**
     * Add an activity in network.     
     */
    public void addActivity(int peso, int inicio, int fim) throws PertException {
        if(!initialized){
            throw new PertException(MSG_UNCONFIGURED_NETWORK);
        }
        addActivity(new Activity(uiSystem, this, peso, inicio, fim, Constants.DEFAULT_CONNECTOR));
    }
    
    public int findNextAvailableEventId(){
        for(int i = 0 ; i < MAX_EVENT_COUNT ; i++){
            if(!eventMap.containsKey(i)){
                return i;
            }
        }
        return MAX_EVENT_COUNT;
    }
    
    public Event newTempEvent(int id){        
        return new Event(null, id);
    }
    
    public void addNewEvent(int eventId){
        if(eventId == MAX_EVENT_COUNT){
            return;
        }        
        final Event event = newEvent(eventId, EventType.EVENT_TYPE_NORMAL);
        eventMap.put(event.id, event);
        repaint();
    }
    
    private Event newEvent(int id, EventType type){
        UIElement eventUI = new UIElement(uiSystem);
        eventUI.setSize(Constants.EVT_CONTAINER_WIDTH, Constants.EVT_CONTAINER_HEIGHT);        
        
        final Event newEvent = new Event(uiSystem, id, type);
        final NewActivityButton newActivityButton = new NewActivityButton(uiSystem, newEvent, this);
        
        eventUI.addChild(newEvent);        
        eventUI.addChild(newActivityButton);
        
        uiSystem.getDesktop().addChild(eventUI);
        
        return newEvent;
    }
    
    private void initializeAdvancedCreateEvent(int eventId, EventType type, int x, int y){        
        Event event = newEvent(eventId, type);
        event.getParent().moveAmount(x, y, true);       
        eventMap.put(eventId, event);
    }
    
    private void initializeAdvanced(int eventCount) throws PertException {        
        this.initialized = false;
        
        if(eventCount < 2){
            throw new PertException("The event count must be greater than 1.");
        }
        
        overrideLateTime = false;
        overrideLateTimeValue = 0.0;
        
        uiSystem.setFocus(uiSystem.getDesktop());
        uiSystem.getDesktop().getChildList().clear();
                
        eventMap.clear();
        activitiesList.clear();                
    }
    
    private void initializeAdvancedDone(Integer eventStart, Integer eventEnd) throws PertException{
        initializeAdvancedDone(eventStart, eventEnd, false);
    }
    
    private void initializeAdvancedDone(Integer eventStart, Integer eventEnd, boolean ignoreChecking) throws PertException{
        if(!ignoreChecking){
            if(eventEnd == null){
                throw new PertException("The initial event was not informed!");       
            }
            if(eventStart == eventEnd){
                throw new PertException("The initial and final event must be different!");
            }
            if(eventStart < 0){
                throw new PertException("The initial event must be greater or equal to 0 (zero)!");
            }
            if(eventEnd < 0){
                throw  new PertException("The final event must be greater or equal to 0 (zero)!");
            }
        }        
        this.eventStart = eventStart;
        this.eventEnd = eventEnd;
        this.initialized = true;
    }
    
    /**
     * Initializa the PERT Network
     */
    public void initialize(int eventCount, Integer eventStart, Integer eventEnd) throws PertException {
        initializeAdvanced(eventCount);        
        if(eventStart == null){
            throw new PertException("The initial event was not informed!");
        }
        if(eventEnd == null){
            throw new PertException("The initial event was not informed!");       
        }
        if(eventStart == eventEnd){
            throw new PertException("The initial and final event must be different!");
        }
        if(eventStart < 0){
            throw new PertException("The initial event must be greater or equal to 0 (zero)!");
        }
        if(eventEnd < 0){
            throw  new PertException("The final event must be greater or equal to 0 (zero)!");
        }
        if(eventEnd > eventCount){
            throw new PertException("The final event should be less than the number of events");
        }
        
        int newX = 0;
        int newY = 0;
        
        //
        // Calculate the amount of events based on the actual draw frame dimension
        // instead of using the backbuffer size
        final int eventPerRow = Math.max(Math.floorDiv(getWidth(), Constants.EVT_CONTAINER_WIDTH), 11);
        
        for(int i = 0 ; i < eventCount ; i++){
            EventType type = EventType.EVENT_TYPE_NORMAL;
            if(i == eventStart){
                type = EventType.EVENT_TYPE_START;
            }else if(i == eventEnd){
                type = EventType.EVENT_TYPE_END;
            }        
            
            initializeAdvancedCreateEvent(i, type, newX, newY);            
            
            newX += Constants.EVT_CONTAINER_WIDTH;
            if(i > 0 && (i+1) % eventPerRow == 0){
                newY += Constants.EVT_CONTAINER_WIDTH;
                newX = 0;
            }                        
        }                
        
        initializeAdvancedDone(eventStart, eventEnd, true);
    }        
        
    public boolean load(File file){
        FileInputStream fis = null;        
        try{
            fis = new FileInputStream(file);
            return load(fis);
        }catch(Throwable t){
            t.printStackTrace();
        }finally{
            if(fis != null){
                try{
                    fis.close();
                }catch(Throwable t){

                }
            }
        }
        return false;
    }
    
    public boolean load(InputStream stream){
        try{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            
            Document doc = docBuilder.parse(stream);
            doc.getDocumentElement().normalize();
            
            Element root = doc.getDocumentElement();
            
            if(!"perttool".equals(root.getNodeName())){
                throw new AssertionError("Invalid XML File !");
            }
                                    
            Integer eventCount = Integer.valueOf(root.getAttribute("event_count"));
            
            if(eventCount < 2){
                throw new AssertionError("Invalid XML File, event need to be > 2");
            }
            if(eventCount > MAX_EVENT_COUNT){
                throw new AssertionError("Invalid XML File, event need to be <= " + MAX_EVENT_COUNT);
            }
            
            int docDecimalPlaces = MIN_DECIMAL_PLACES;
            if(root.hasAttribute("decimal_places")){
                docDecimalPlaces = Integer.valueOf(root.getAttribute("decimal_places"));
            }
            
            Integer startEvent = Integer.valueOf(root.getAttribute("start_event"));
            Integer endEvent = Integer.valueOf(root.getAttribute("end_event"));
            
            //System.out.println("EventCount: " + eventCount + " - Start->End " + startEvent  + " -> " + endEvent);
            
            if(startEvent < 0){
                throw new AssertionError("Invalid XML File, start_event must be >= 0");
            }
            if(endEvent < 0){
                throw new AssertionError("Invalid XML File, end_event must be >= 0");
            }
            if(Objects.equals(startEvent, endEvent)){
                throw new AssertionError("Invalid XML File, end_event must be != start_event !");
            }
                                                
            NodeList nodeEventList = root.getElementsByTagName("eventList");            
            Element eventListElem = (Element)nodeEventList.item(0);
            NodeList eventItemList = eventListElem.getChildNodes();
                        
            initializeAdvanced(eventCount);
            
            int maxX = getWidth(), maxY = getHeight();
            
            for(int i = 0 ; i < eventItemList.getLength() ; i++){
                final Node node = eventItemList.item(i);
                if(!(node instanceof Element)){
                    continue;
                }
                final Element eventElem = (Element)node;                
                
                Integer id = Integer.valueOf(eventElem.getAttribute("id"));
                Integer x = Integer.valueOf(eventElem.getAttribute("x"));
                Integer y = Integer.valueOf(eventElem.getAttribute("y"));
                
                int winAddX = 0, winAddY = 0;
                int evtMaxX = x + Constants.EVT_CONTAINER_WIDTH;
                int evtMaxY = y + Constants.EVT_CONTAINER_HEIGHT;
                
                if(evtMaxX > maxX){
                    winAddX = (evtMaxX - maxX) + 20; // + 20 some extra padding
                    maxX = evtMaxX;                    
                }
                if(evtMaxY > maxY){
                    winAddY = (evtMaxY - maxY) + 20;  // + 20 some extra padding
                    maxY = evtMaxY;
                }
                
                if(winAddX != 0 || winAddY != 0){
                    Container frame = getParent().getParent().getParent().getParent().getParent();
                                        
                    int newWidth = frame.getWidth() + winAddX;
                    int newHeight = frame.getHeight() + winAddY;
                    
                    uiSystem.getDesktop().setSize(newWidth, newHeight);
                    
                    frame.setSize(newWidth, newHeight);
                }
                
                // TODO: Check if valid id
                
                EventType type = EventType.EVENT_TYPE_NORMAL;
                if(id == startEvent){
                    type = EventType.EVENT_TYPE_START;
                }else if(id == endEvent){
                    type = EventType.EVENT_TYPE_END;
                }                
                
                initializeAdvancedCreateEvent(id, type, x, y);
            }            
            
            initializeAdvancedDone(startEvent, endEvent);
            
            NodeList nodeActivity = root.getElementsByTagName("activityList");
            Element activityList = (Element)nodeActivity.item(0);
            NodeList activityListChild = activityList.getChildNodes();
            // No Activities, done !
            if(activityListChild.getLength() == 0){
                return true;
            }
            
            for(int i = 0 ; i < activityListChild.getLength() ; i++){
                final Node node = activityListChild.item(i);
                if(!(node instanceof Element)){
                    continue;
                }
                final Element activityEleme = (Element)node;
                                
                Integer src = Integer.valueOf(activityEleme.getAttribute("src"));
                Integer dst = Integer.valueOf(activityEleme.getAttribute("dst"));
                Double weight = Double.valueOf(activityEleme.getAttribute("weight").replace(',', '.'));
                ConnectorType connectorType = ConnectorType.CONNECTOR_TYPE_LINE;
                                
                if(activityEleme.hasAttribute("connectorType")){
                    try{
                        ConnectorType connectorTypeFromXML = ConnectorType.valueOf(activityEleme.getAttribute("connectorType"));
                        connectorType = connectorTypeFromXML;
                    }catch(Throwable t){
                        t.printStackTrace(System.err);
                    }
                }
                
                Activity activity = new Activity(uiSystem, this, weight, src, dst, connectorType);
                
                // Try to load the language attribute instead of desc directly
                // to allow easy internationalization of examples.
                // 
                // <.. desc="English Text" desc_pt="Texto Portugues" ... />
                //
                // "desc" should always have english text this way english will be, 
                // loaded for not translated languages, "desc_pt" Portuguese Brazil, ...
                String intDesc = "desc_" + Internationalization.getLanguageDir();                
                if(activityEleme.hasAttribute(intDesc)){
                    activity.setDescription(activityEleme.getAttribute(intDesc));
                }else{                
                    if(activityEleme.hasAttribute("desc")){
                        activity.setDescription(activityEleme.getAttribute("desc"));
                    }
                }
                addActivity(activity, true);
            }
            
            setDecimalPlaces(docDecimalPlaces);
            recalculateNetworkTime();            
            
            return true;
        }catch(GraphCycleException e){
            e.printStackTrace();
        }catch(Throwable t){
            t.printStackTrace();
        }
        return false;
    }
    
    public boolean save(File file){
        try{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            
            Document doc = docBuilder.newDocument();
            Element root = doc.createElement("perttool");
            doc.appendChild(root);
                        
            root.setAttribute("event_count", String.valueOf(eventMap.size()));
            root.setAttribute("decimal_places", String.valueOf(decimalPlaces));
            root.setAttribute("start_event", String.valueOf(eventStart));
            root.setAttribute("end_event", String.valueOf(eventEnd));
                        
            Element eventListElem = doc.createElement("eventList");
            root.appendChild(eventListElem);
            
            for(Event event : eventMap.values()){
                Element eventElem = doc.createElement("event");
                eventListElem.appendChild(eventElem);
                
                eventElem.setAttribute("id", String.valueOf(event.id));
                
                UIElement parent = event.getParent();
                int x = parent.getViewPosition().getX();
                int y = parent.getViewPosition().getY();
                
                eventElem.setAttribute("x", String.valueOf(x));
                eventElem.setAttribute("y", String.valueOf(y));
            }
            
            Element activityListElem = doc.createElement("activityList");
            root.appendChild(activityListElem);
            
            for(Activity activity : activitiesList){
                Element activityElem = doc.createElement("activity");
                activityListElem.appendChild(activityElem);
                activityElem.setAttribute("src", String.valueOf(activity.eventStart));
                activityElem.setAttribute("dst", String.valueOf(activity.eventEnd));
                activityElem.setAttribute("weight", String.valueOf(activity.getWeight()));
                activityElem.setAttribute("connectorType", activity.getConnectorType().name());
                if(activity.hasDescription()){
                    activityElem.setAttribute("desc", activity.getDescription());
                }
            }            
            
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(file);
            
            transformer.transform(source, result);
            
            return true;
        }catch(Throwable t){
            t.printStackTrace();
        }
        return false;
    }
    
    public boolean exportImage(File fileOut, ExportFormat format){
        imageExporting = true;
        boolean result = imageGenerator.exportToImage(fileOut, this, format);
        imageExporting = false;
        return result;
    }
    
    private void renderToDrawCache(){
        try{
            imageGenerator.drawDiagramFor(drawCache.getGraphics(), this, BACKBUFFER_WIDTH, BACKBUFFER_HEIGHT);
        }catch(Throwable t){
            t.printStackTrace(System.err);
        }
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);        
        if(initialized){            
            renderToDrawCache();
            g.drawImage(drawCache, 0, 0, this);
        }else{
            g.setColor(Color.GRAY);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }    
    
    public void showEditActivityDialogFor(Activity activity, boolean editingNewActivity){
        JDManageActivity editActivityDialog = new JDManageActivity(this, activity, editingNewActivity, null, true);
        editActivityDialog.setVisible(true);        
        repaint();
    }    
}
