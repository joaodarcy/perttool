/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public final class ResourceManager {
        
    public final static BufferedImage ARROW_IMAGE;
    public final static BufferedImage EVENT_IMAGE;    
    public final static BufferedImage EVENT_START_IMAGE;
    public final static BufferedImage EVENT_END_IMAGE;
    public final static BufferedImage PLUS_IMAGE;
    public final static BufferedImage PLUS_IMAGE_OVER;
    public final static Image LOGO;
    public final static BufferedImage APP_LOGO;
    
    public final static ImageIcon LANG_PT_BR;
    public final static ImageIcon LANG_EN_US;
    public final static ImageIcon LANG_ES;
    
    public final static ImageIcon ICON_ZOOM_IN;
    public final static ImageIcon ICON_ZOOM_OUT;
    public final static ImageIcon ICON_EDIT;
    public final static ImageIcon ICON_FILE;
    public final static ImageIcon ICON_SAVE;
    public final static ImageIcon ICON_TRASH;
    public final static ImageIcon ICON_CLOCK;
    public final static ImageIcon ICON_FONT;
    public final static ImageIcon ICON_PAINT_BRUSH;
    public final static ImageIcon ICON_FOLDER_OPEN;
    public final static ImageIcon ICON_INFO_CIRCLE;
    public final static ImageIcon ICON_ADD_EVENT;
    public final static ImageIcon ICON_CALCULATOR;
    public final static ImageIcon ICON_DECIMAL_PLACES;
    public final static ImageIcon ICON_UNDO;
    public final static ImageIcon ICON_REDO;
    public final static ImageIcon ICON_EVALUATION;
    
    public final static ImageIcon ICON_STRAIGHT_LINE;
    public final static ImageIcon ICON_CUBIC_BEZIER;
    
    static{
        ARROW_IMAGE         = LoadImage("drawable/arrow.png");
        EVENT_IMAGE         = LoadImage("drawable/event_normal.png");
        EVENT_START_IMAGE   = LoadImage("drawable/event_start.png");
        EVENT_END_IMAGE     = LoadImage("drawable/event_end.png");
        PLUS_IMAGE          = LoadImage("drawable/plus.png");
        PLUS_IMAGE_OVER     = LoadImage("drawable/plus_over.png");
        LOGO                = LoadImage("drawable/logo_pert_tool.png").getScaledInstance(PertImageGenerator.LOGO_WIDTH, PertImageGenerator.LOGO_HEIGHT, Image.SCALE_SMOOTH);
        APP_LOGO            = LoadImage("drawable/app_icon.png");
        
        LANG_PT_BR          = LoadIcon("drawable/icons_country/br.png");
        LANG_EN_US          = LoadIcon("drawable/icons_country/us.png");
        LANG_ES             = LoadIcon("drawable/icons_country/es.png");
        
        ICON_ZOOM_IN        = LoadIcon("drawable/icons_menu/zoom_in.png");
        ICON_ZOOM_OUT       = LoadIcon("drawable/icons_menu/zoom_out.png");        
        ICON_EDIT           = LoadIcon("drawable/icons_menu/edit.png");
        ICON_FILE           = LoadIcon("drawable/icons_menu/file.png");
        ICON_SAVE           = LoadIcon("drawable/icons_menu/save.png");
        ICON_TRASH          = LoadIcon("drawable/icons_menu/trash.png");
        ICON_CLOCK          = LoadIcon("drawable/icons_menu/clock.png");
        ICON_FONT           = LoadIcon("drawable/icons_menu/font.png");
        ICON_PAINT_BRUSH    = LoadIcon("drawable/icons_menu/paint_brush.png");
        ICON_FOLDER_OPEN    = LoadIcon("drawable/icons_menu/folder_open.png");
        ICON_INFO_CIRCLE    = LoadIcon("drawable/icons_menu/info_circle.png");               
        ICON_ADD_EVENT      = LoadIcon("drawable/icons_menu/add_event.png");
        ICON_CALCULATOR     = LoadIcon("drawable/icons_menu/calculator.png");
        ICON_DECIMAL_PLACES = LoadIcon("drawable/icons_menu/decimal_places.png");
        ICON_UNDO           = LoadIcon("drawable/icons_menu/undo_icon.png");
        ICON_REDO           = LoadIcon("drawable/icons_menu/redo_icon.png");
        ICON_EVALUATION     = LoadIcon("drawable/icons_menu/evaluation.png");
        
        ICON_STRAIGHT_LINE  = LoadIcon("drawable/icons_menu/line.png");
        ICON_CUBIC_BEZIER   = LoadIcon("drawable/icons_menu/curve.png");
    }

    private ResourceManager() {
        throw new AssertionError();
    }
            
    public static BufferedImage LoadImage(String path){
        try{            
            InputStream res = ResourceManager.class.getClassLoader().getResourceAsStream(path);            
            return ImageIO.read(res);
        }catch(Throwable t){
            t.printStackTrace();
        }
        return null;
    }
    
    private static ImageIcon LoadIcon(String path){
        try{            
            URL res = ResourceManager.class.getClassLoader().getResource(path);            
            return new ImageIcon(res);
        }catch(Throwable t){
            t.printStackTrace();
        }
        return null;
    }    
}
