/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import br.com.joaodarcy.perttool.cmd.Command;
import java.util.Stack;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public final class Application {
    
    private final static PertDocument doc;
    
    private final static Stack<Command> undoStack;
    private final static Stack<Command> redoStack;
    
    private static CommandHistoryListener listener;
    
    static {
        listener = null;
        doc = new PertDocument(300, 300);
        undoStack = new Stack<>();
        redoStack = new Stack<>();
    }

    public static void setCommandHistoryListener(CommandHistoryListener newListener){
        listener = newListener;
        
        // So the listener knows the current state
        notifyListeners();
    }
    
    public static void clearCommandHistory(){
        undoStack.clear();
        redoStack.clear();
        notifyListeners();
    }
    
    public static void storeAndExecute(Command cmd){
        assert(cmd != null);
        cmd.execute();
        undoStack.push(cmd);
        redoStack.clear();
        
        notifyListeners();
    }
    
    public static boolean isRedoAvailable(){
        return !redoStack.isEmpty();
    }
    
    public static boolean isUndoAvailable(){
        return !undoStack.isEmpty();
    }
    
    public static void redo(){
        if(isRedoAvailable()){
            Command cmd = redoStack.pop();
            undoStack.push(cmd);
            cmd.execute();

            notifyListeners();
        }
    }
    
    public static void undo(){
        if(isUndoAvailable()){            
            Command cmd = undoStack.pop();
            redoStack.push(cmd);
            cmd.unExecute();

            notifyListeners();
        }
    }
        
    private static void notifyListeners(){
        if(listener != null){
            listener.undoAvailable(!undoStack.isEmpty());
            listener.redoAvailable(!redoStack.isEmpty());
        }
    }    
    
    public static PertDocument getDoc() {
        return doc;
    }            
}
