/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import br.com.joaodarcy.perttool.desenhar.Constants;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** 
 * Alpha Version
 * 
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class GanttGenerator {
        
    private final TopologySort topologySort;

    private final PertDocument doc;
    private double minWeight;
    private double maxWeight;
    private final double padding;
    private final Color backgroundColor;
    private final Color foregroundColor;
    private final Color barBackgroundColor;
    private final Color barBorderColor;
    private final Color lineColor;
    
    private double remainWidth;
    private int diagramHeight;
    private int headerHeight;
    private int halfPadding;
    private double x0;
    
    private FontMetrics metrics;
    
    public GanttGenerator() {
        this(new DFSTopologySort());
    }

    public GanttGenerator(TopologySort topologySort) {
        this.topologySort = topologySort;
        this.padding = 16.0;
        this.halfPadding = (int)(padding / 2.0);
        this.backgroundColor = new Color(255, 255, 204);
        this.foregroundColor = Color.BLACK;
        this.barBackgroundColor = new Color(91, 155, 213);
        this.barBorderColor = Color.BLACK;
        this.lineColor = Color.GRAY;
        this.doc = Application.getDoc();
    }        
                
    private double normalizeWeight(double weight){
        return (weight - minWeight) / (maxWeight - minWeight);
    }
    
    private void drawSeparatorFor(Graphics2D g, double value){        
        g.setColor(lineColor);
        g.setStroke(Constants.ARROW_LINE_PHANTHOM_STROKE);
        double x = x0 + normalizeWeight(value) * remainWidth;
        g.drawLine((int)x, headerHeight, (int)x, diagramHeight);
                
        g.setColor(foregroundColor);
        g.setStroke(Constants.ARROW_LINE_STROKE);
        String valueString = String.format(doc.getAcitivyWeightFormatString(), value);
        int width = metrics.stringWidth(valueString);
        x = x0 + normalizeWeight(value) * remainWidth;
        if(value == maxWeight){
            g.drawString(valueString, (int)(x - width - 2), headerHeight - halfPadding);
        }else{
            g.drawString(valueString, (int)x - (width / 2), headerHeight - halfPadding);
        }
    }
    
    private void drawLanes(Graphics2D g, int lineHeight, int firstColumnWidth, double incRate, double minValue, double maxValue, int diagramHeight, List<Integer> sortedList, int diagramWidth){        
        headerHeight = (2 * lineHeight);
                    
        remainWidth = diagramWidth - firstColumnWidth;
        
        x0 = firstColumnWidth;                        
        double headerWidth = remainWidth;
        
        g.setColor(backgroundColor);
        g.fillRect(firstColumnWidth, 0, (int)headerWidth, headerHeight);        
        
        g.setColor(foregroundColor);
        g.drawRect(firstColumnWidth, 0, (int)headerWidth, headerHeight);
        
        g.drawLine(firstColumnWidth, diagramHeight, (int)(firstColumnWidth + headerWidth), diagramHeight);
               
        // Draw Title
        String durationTitle = Internationalization.getWordWeight();
        int titleWidth = metrics.stringWidth(durationTitle);
        
        g.drawString(durationTitle, firstColumnWidth + (int)(remainWidth / 2.0) - (titleWidth / 2) - halfPadding, lineHeight - halfPadding);        
           
        // Draw line separator
        {
            g.setColor(foregroundColor);
            List<Double> rangeList = new ArrayList<>();
            for(Integer eventId : sortedList){
                Event event = doc.getEventById(eventId);
                double startValue = event.getEarlyTime();

                if(startValue != 0.0){
                    if(!rangeList.contains(startValue)){
                        rangeList.add(startValue);
                    }
                }

                for(Activity activity : doc.getStartingActivitesInEvent(eventId)){
                    if(activity.isPhatom()){
                        continue;
                    }
                    double nextStep = startValue + activity.getWeight();
                    if(!rangeList.contains(nextStep)){
                        rangeList.add(nextStep);
                        drawSeparatorFor(g, nextStep);
                    }
                }
            }
        }
        
        // Draw final line as solid black
        g.setStroke(Constants.ARROW_LINE_STROKE);
        g.setColor(foregroundColor);
        double x = (x0 + normalizeWeight(maxValue) * remainWidth) - 1;
        g.drawLine((int)x, 0, (int)x, diagramHeight);
        
        int lineNumber = -1;
        
        // Draw Activities
        for(Integer eventId : sortedList){
            Event event = doc.getEventById(eventId);
            double startValue = event.getEarlyTime();
            
            for(Activity activity : doc.getStartingActivitesInEvent(eventId)){
                if(activity.isPhatom()){
                    continue;
                }
               
                lineNumber++;

                int lineY = (lineHeight * 2) + (lineNumber * lineHeight + halfPadding);
                
                double startX = x0 + (normalizeWeight(startValue) * remainWidth);                
                double width = (normalizeWeight(activity.getWeight()) * remainWidth);
                
                g.setColor(barBackgroundColor);
                g.fillRect((int)startX, lineY, (int)width, lineHeight - halfPadding);
                
                g.setColor(barBorderColor);
                g.drawRect((int)startX, lineY, (int)width, lineHeight - halfPadding);
                               
            }
        }        
    }
    
    public void draw(Graphics2D g, int diagramWidth){
        final List<Integer> sortedList = new ArrayList<>(topologySort.sort(doc));
                        
        Collections.reverse(sortedList);
        
        PertImageGenerator.enableHighQualityFor(g);
        
        final Font oldFont = g.getFont();
        
        g.setFont(new Font("Arial", Font.PLAIN, 18));
        g.setStroke(Constants.ARROW_LINE_STROKE);
        
        metrics = g.getFontMetrics();
        
        int activityWidth = 0, activityCount = 0;        
        int lineHeight = metrics.getHeight() + halfPadding;
        
        for(Integer eventId : sortedList){                                   
            for(Activity activity : doc.getStartingActivitesInEvent(eventId)){
                final String description = activity.getDescription();
                                
                if(activity.isPhatom()){
                    continue;
                }                
                
                activityCount++;
                                
                if(description == null || description.isEmpty()){
                    continue;
                }
                int curWidth = metrics.stringWidth(description) + halfPadding;
                if(curWidth > activityWidth){
                    activityWidth = curWidth;
                }
            }
        }
        
        activityWidth = activityWidth + (int)padding;
        
        minWeight = doc.getEventById(doc.eventStart).getEarlyTime();
        maxWeight = doc.getEventById(doc.eventEnd).getEarlyTime();        
        
        diagramHeight = ((activityCount + 2) * lineHeight + (int)padding);
        
        {            
            double incrementRate = ((minWeight + maxWeight) / (double)activityCount + 1);
            
            drawLanes(g, lineHeight, activityWidth, incrementRate, minWeight, maxWeight, diagramHeight, sortedList, diagramWidth);                        
        }
                        
        // 1. Draw the Background
        g.setColor(backgroundColor);
        g.fillRect(0, 0, activityWidth, diagramHeight);
        
        g.setColor(foregroundColor);
        g.drawRect(0, 0, activityWidth, diagramHeight);
        
        // 2. Draw Title
                        
        String activityTitle = Internationalization.getWordActivity();
        int titleWidth = metrics.stringWidth(activityTitle);
        
        g.drawString(activityTitle, (activityWidth / 2) - (titleWidth / 2) + halfPadding, lineHeight);
        
        {
            int lineNumber = -1;
                
            // 3. Draw Activities
            for(Integer eventId : sortedList){
                //System.out.println(i);            
                for(Activity activity : doc.getStartingActivitesInEvent(eventId)){
                    final String description = activity.getDescription();
                    
                    if(activity.isPhatom()){
                        continue;
                    }
                    
                    lineNumber++;
                    
                    if(description == null || description.isEmpty()){
                        continue;
                    }

                    int lineY = (lineHeight * 3) + (lineNumber * lineHeight + halfPadding);
                    g.drawString(description, halfPadding, (int)(lineY - padding));
                }
            }        
        }
                
        g.setFont(oldFont);
    }
        
}
