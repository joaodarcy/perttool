/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.ui;

import java.awt.Graphics2D;
import java.awt.Image;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class UIRect extends UIElement {

    private Image image;
    
    public UIRect(UISystem uiSystem) {
        super(uiSystem);
    }

    public void setImage(Image image) {
        this.image = image;
    }   
    
    @Override
    public void render(Graphics2D g) {
        if(image != null){
            g.drawImage(image, viewPosition.x, viewPosition.y, size.x, size.y, null);
        }
        super.render(g);
    }              
}
