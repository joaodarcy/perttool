/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.ui;

import br.com.joaodarcy.perttool.math.Vector2i;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class UIElement implements MouseMotionListener, MouseListener, KeyListener {
    
    protected final UISystem uiSystem;
    protected UIElement parent;
            
    protected Vector2i size;
    protected Vector2i clientPosition;
    protected Vector2i viewPosition;
    
    protected List<UIElement> childList;

    protected boolean draggable;
    protected boolean dragging;
    protected boolean moveParentOnDrag;    
    
    protected Vector2i lastMouseInView;
    
    public UIElement(UISystem uiSystem) {
        this.uiSystem = uiSystem;
        this.childList = new ArrayList<>();
        
        // Dragging Stuff
        this.dragging = false;
        this.moveParentOnDrag = false;
        this.draggable = true;
        this.lastMouseInView = new Vector2i();        
        
        this.clientPosition = new Vector2i();
        this.viewPosition = new Vector2i();
        this.size = new Vector2i();
    }                    
   
    protected void onGainFocus(){
        
    }
    
    protected void onLoseFocus(UIElement elementGainFocus){
        
    }

    public UIElement getParent() {
        return parent;
    }
           
    public void addChild(UIElement newChild){
        if(newChild == null){
            return;
        }
        if(newChild.parent != null){
            throw new IllegalStateException("The UIElement already have a parent !");
        }
        newChild.parent = this;
        childList.add(newChild);
    }

    public List<UIElement> getChildList() {
        return childList;
    }   

    public Vector2i getViewPosition() {
        return viewPosition;
    }   
    
    public Vector2i getClientPosition() {
        return clientPosition;
    }        
    
    public Vector2i getSize(){
        return size;
    }
            
    public void removeChild(UIElement child){
        if(child == null){
            return;
        }
        if(child.parent == null){
            // Should never happen !
            throw new AssertionError("The element doesn't have a parent !");
        }
        childList.remove(child);
        child.parent = null;
    }
    
    public void render(Graphics2D g){
        // Reverse order render
        for(int i = childList.size() - 1 ; i >= 0 ; i--){
            UIElement child = childList.get(i);
            child.render(g);
        }        
    }
    
    public boolean containsPoint(int x, int y){
        return containsPoint(x, y, true);
    }
    
    public boolean containsPoint(int x, int y, boolean checkChild){
        if(Rectangle.containsPoint(viewPosition, size, x, y)){
            return true;
        }
        if(checkChild){
            for(UIElement child : childList){
                if(child.containsPoint(x, y, checkChild)){
                    return true;
                }
            }
        }
        return false;
    }
    
    public UIElement getElementAt(int x, int y){                
        if(!containsPoint(x, y)){
            return null;
        }
        for(UIElement child : childList){
            UIElement elementOver = child.getElementAt(x, y);
            if(elementOver != null){
                return elementOver;
            }
        }
        return this;
    }
            
    protected void updateViewPosition(){
        viewPosition.x = clientPosition.x;
        viewPosition.y = clientPosition.y;
        
        clientPositionToViewPosition(viewPosition);
        
        for(UIElement e : childList){
            e.updateViewPosition();
        }
    }        
    
    protected void clientPositionToViewPosition(Vector2i clientPosition){
        if(parent == null){
            return;
        }        
        
        clientPosition.x += parent.clientPosition.x;
        clientPosition.y += parent.clientPosition.y;
        
        parent.clientPositionToViewPosition(clientPosition);
    }
    
    public void moveAmount(int deltaX, int deltaY){
        moveAmount(deltaX, deltaY, false);
    }
    
    public void moveAmount(int deltaX, int deltaY, boolean force){
        // Prevents Desktop to be dragged, or not moved components.
        if(parent == null || (deltaX == 0 && deltaY == 0 && !force)){
            return;
        }
        
        if(!draggable){
            if(moveParentOnDrag){
                parent.moveAmount(deltaX, deltaY, force);                
            }
            return;
        }

        int newX = clientPosition.x + deltaX;
        int newY = clientPosition.y + deltaY;
        
        // Avoid getting out of parent bounds
        final Vector2i parentSize = parent.getSize();
        if(newX + size.x > parentSize.x){
            newX = parentSize.x - size.x;
        }
        if(newX < 0){
            newX = 0;
        }
        if(newY + size.y > parentSize.y){
            newY = parentSize.y - size.y;
        }
        if(newY < 0){
            newY = 0;
        }

        clientPosition.set(newX, newY);
        updateViewPosition();
                                
        uiSystem.setViewDirty();
    }
                
    public void setPosition(int newX, int newY){                
        int deltaX = newX - clientPosition.x;
        int deltaY = newY - clientPosition.y;
        
        moveAmount(deltaX, deltaY);
    }
    
    public void setSize(int newWidth, int newHeight){
        size.set(newWidth, newHeight);
    }
    
    @Override
    public void mouseDragged(MouseEvent e) {
        if(dragging){            
            int deltaMoveX = e.getX() - lastMouseInView.x;
            int deltaMoveY = e.getY() - lastMouseInView.y;
            
            moveAmount(deltaMoveX, deltaMoveY);
            
            lastMouseInView.set(e.getX(), e.getY());
        }
    }

    protected void onDragBegin(){
        
    }
    
    protected void onDragEnd(){
        
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
        // Nothing todo
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        throw new UnsupportedOperationException("MouseClicked should not be used, use mousePressed/mouseReleased instead !");
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1 || e.getButton() == MouseEvent.BUTTON3){ // Left/Right Button
            // Get the clicked element
            UIElement element = uiSystem.getDesktop().getElementAt(e.getX(), e.getY());
            // Forward the event if needed
            if(element != null && element != this){
                if(uiSystem.setFocus(element)){
                    element.mousePressed(e);                    
                    return;
                }
            }            
            onDragBegin();
            dragging = true;
            lastMouseInView.set(e.getX(), e.getY());
        }        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1){
            if(dragging){
                onDragEnd();
                dragging = false;
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        throw new AssertionError("Not used for now");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        throw new AssertionError("Not used for now");
    }

    public void setDraggable(boolean draggable) {
        this.draggable = draggable;
    }

    public boolean isDraggable() {
        return draggable;
    }        

    @Override
    public void keyTyped(KeyEvent e) {
        // Not Used
    }

    @Override
    public void keyPressed(KeyEvent e) {
        // Not Used
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // Not Used
    }
}
