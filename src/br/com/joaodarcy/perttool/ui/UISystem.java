/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.ui;

import br.com.joaodarcy.perttool.PertDocument;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class UISystem implements MouseListener, MouseMotionListener, KeyListener {
    
    protected List<UIListener> eventListenerList;
    
    protected UIElement desktop;
    protected UIElement focused;
    protected UIElement mouseOver;
    protected PertDocument rede;
    protected boolean viewDirty;

    public UISystem(PertDocument rede, int width, int height) {
        this.rede = rede;
        this.desktop = new UIElement(this);
        this.desktop.setSize(width, height);
        this.eventListenerList = new ArrayList<>();
        
        this.focused = this.desktop;
        this.mouseOver = this.desktop;
        this.viewDirty = true;
    }                
    
    public void addUIListener(UIListener listener){
        if(listener != null){
            if(!eventListenerList.contains(listener)){
                eventListenerList.add(listener);
            }
        }        
    }        
    
    public boolean setFocus(UIElement element){
        if(element == focused){
            return true;
        }
        // Not needed yet
        UIElement lostFocus = focused;
        lostFocus.onLoseFocus(element);
        
        focused = element;
        // Not needed yet
        element.onGainFocus();
        
        setViewDirty();
        
        for(UIListener listener : eventListenerList){
            listener.onSelectedItem(element);
        }
        
        return true;
    }
    
    private void redrawView(){
        if(viewDirty){
            rede.repaint();
            viewDirty = false;
        }
    }
    
    public void setViewDirty(){
        viewDirty = true;
    }
    
    public UIElement getDesktop() {
        return desktop;
    }   

    public UIElement getFocused() {
        return focused;
    }        
    
    public UIElement getMouseOver() {
        return mouseOver;
    }

    public void render(Graphics2D g){
        desktop.render(g);
    }
        
    @Override
    public void mouseClicked(MouseEvent e) {
        //System.out.println("MouseClicked");
        // Ignore :D
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //System.out.println("MousePressed");
        focused.mousePressed(e);        
        redrawView();        
    }

    @Override
    public void mouseReleased(MouseEvent e) { 
        //System.out.println("MouseReleased");
        // Not needed for now
        focused.mouseReleased(e);        
        redrawView();        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        rede.requestFocus();
        //System.out.println("MouseEntered");
        // Ignore :D
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //System.out.println("MouseExited");
        // Ignore :D
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        focused.mouseDragged(e);        
        redrawView();
    }

    @Override
    public void mouseMoved(MouseEvent e) {               
        focused.mouseMoved(e);
        
        UIElement oldMouseOver = mouseOver;
        UIElement newMouseOver = desktop.getElementAt(e.getX(), e.getY());
        
        if(newMouseOver != null && newMouseOver != mouseOver){
            mouseOver = newMouseOver;
        }
        if(oldMouseOver != focused){
            oldMouseOver.mouseMoved(e);
        }
        
        redrawView();        
    }

    @Override
    public void keyTyped(KeyEvent e) {
        focused.keyTyped(e);
        redrawView();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        focused.keyPressed(e);
        redrawView();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        focused.keyReleased(e);
        redrawView();
    }
    
}
