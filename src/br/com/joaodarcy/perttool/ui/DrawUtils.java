/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.ui;

import br.com.joaodarcy.perttool.math.Vector2i;
import br.com.joaodarcy.perttool.desenhar.Constants;
import br.com.joaodarcy.perttool.math.Vector2f;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Path2D;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class DrawUtils {

    public static float CURVE_CONTROL_POINT_SIZE = 50.0f;
    
    public static double angleCubic;
    
    public enum ConnectorType {
        CONNECTOR_TYPE_LINE,
        CONNECTOR_TYPE_CUBIC_BEZIER;
    };
    
    private DrawUtils() {
        throw new IllegalStateException();
    }
    
    public static void drawConnector(Graphics2D g, Stroke stroke, Vector2f p0, Vector2i p1){
        drawConnector(g, stroke, p0, new Vector2f(p1.x, p1.y), Constants.DEFAULT_CONNECTOR, true);
    }
    
    public static Vector2f drawConnector(Graphics2D g, Stroke stroke, Vector2f p0, Vector2f p1, ConnectorType type, boolean isNewNode){
        g.setStroke(stroke);
        
        p0.set(getIntersectionPointFor(p0, p1));
        
        Vector2f p1Center;
        
        if(isNewNode == false){
            // Make a copy of final position center value before we change it
            p1Center = new Vector2f(p1);
            p1.set(getIntersectionPointFor(p1, p0));
        }else{
            p1Center = p1;
        }
        
        if(type == ConnectorType.CONNECTOR_TYPE_LINE){
            return drawStraightLine(g, p0, p1);
        }else{
            return drawCurve(g, p0, p1, p1Center);
        }
    }
    
    private static Vector2f getIntersectionPointFor(final Vector2f p0, final Vector2f p1){
        final Vector2f diff = p0.subtract(p1).getNormalized();
        final float cosValue = diff.dot(Vector2f.X_AXIS) / diff.getLength();
        final double angle = Math.acos(cosValue);

        final float dirY = p0.y > p1.y ? -1.0f : 1.0f;
        
        return new Vector2f(p0.x - ((float)cosValue * Constants.EVT_HALF_RADIUS_FLOAT), p0.y + ((float)Math.sin(angle) * Constants.EVT_HALF_RADIUS_FLOAT) * dirY);
    }
    
    private static Vector2f drawStraightLine(Graphics2D g, Vector2f p0, Vector2f p1){
        g.drawLine((int)p0.x, (int)p0.y, (int)p1.x, (int)p1.y);
        return p0;
    }
    
    private static Vector2f getCubicBezierPointAt(float t, Vector2f p0, Vector2f cp0, Vector2f cp1, Vector2f p1){
        return new Vector2f(
            (1-t)*(1-t)*(1-t)*p0.x + 3*(1-t)*(1-t)*t*cp0.x + 3*(1-t)*t*t*cp1.x + t*t*t*p1.x,
            (1-t)*(1-t)*(1-t)*p0.y + 3*(1-t)*(1-t)*t*cp0.y + 3*(1-t)*t*t*cp1.y + t*t*t*p1.y
        );
    }
        
    // FIXME: We need to calculate the Arrow Rotation correctly
    private static Vector2f drawCurve(Graphics2D g, Vector2f p0, Vector2f p1, Vector2f p1Center){
        Path2D path = new Path2D.Float();
        
        final Vector2f cp0 = new Vector2f(p0.x + CURVE_CONTROL_POINT_SIZE, p0.y);
        final Vector2f cp1 = new Vector2f(p1.x - CURVE_CONTROL_POINT_SIZE, p1.y);
        
        path.moveTo(p0.x, p0.y);
        path.curveTo(cp0.x, cp0.y, cp1.x, cp1.y, p1.x, p1.y);
        
        g.draw(path);
        
        final Vector2f anglePoint = getCubicBezierPointAt(0.928f, p0, cp0, cp1, p1);
        p1.set(getIntersectionPointFor(anglePoint, p1Center));
        
        return anglePoint;
    }
}
