/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.ui;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public abstract class UIButton extends UIElement {

    protected boolean mouseClickDown;
    protected boolean mouseOver;
    
    protected Image imageNormal;
    protected Image imageOver;
    
    public UIButton(UISystem uiSystem) {
        super(uiSystem);
        mouseClickDown = false;
        mouseOver = false;
    }        

    public void setImageNormal(Image imageNormal) {
        this.imageNormal = imageNormal;
    }

    public void setImageOver(Image imageOver) {
        this.imageOver = imageOver;
    }
        
    protected abstract void onMouseClick(MouseEvent e);
    
    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1){
            mouseClickDown = true;
        }
        super.mousePressed(e);
    }   
    
    @Override
    public void mouseReleased(MouseEvent e) {        
        if(e.getButton() == MouseEvent.BUTTON1){
            if(mouseClickDown && containsPoint(e.getX(), e.getY())){
                onMouseClick(e);
            }
            mouseClickDown = false;
        }        
        super.mouseReleased(e);
    }          

    @Override
    public void render(Graphics2D g) {
        Image image = imageNormal;
        if(mouseOver){
            if(imageOver != null){
                image = imageOver;
            }
        }
        g.drawImage(image, viewPosition.x, viewPosition.y, size.x, size.y, null);
        super.render(g);
    }  

    @Override
    public void mouseMoved(MouseEvent e) {
        if(uiSystem.getMouseOver() == this){
            if(!mouseOver){
                mouseOver = true;
                uiSystem.setViewDirty();
            }
        }else{
            if(mouseOver){
                uiSystem.setViewDirty();
            }
            mouseOver = false;
        }        
        super.mouseMoved(e);
    }        
}
