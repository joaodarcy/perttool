/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.ui;

import br.com.joaodarcy.perttool.math.Vector2f;
import br.com.joaodarcy.perttool.math.Vector2i;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class Rectangle {
    int x;
    int y;
    int width;
    int height;
    
    public Vector2i getCenter(){
        return new Vector2i(x + width / 2, y + height / 2);
    }
    
    public Vector2i getPosition(){
        return new Vector2i(x, y);
    }
    
    public Vector2i getSize(){
        return new Vector2i(width, height);
    }
    
    public void setPosition(int newX, int newY){
        x = newX;
        y = newY;
    }
    
    public void setSize(int newWidth, int newHeight){
        width = newWidth;
        height = newHeight;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }       
    
    public static Vector2i getCenter(final UIElement e){
        return getCenter(e.viewPosition, e.size);
    }
    
    public static Vector2f getCenterEx(final UIElement e){
        return new Vector2f((float)e.viewPosition.x + (float)e.size.x / 2.0f, (float)e.viewPosition.y + (float)e.size.y / 2.0f);
    }
    
    public static Vector2i getCenter(final Vector2i position, final Vector2i size){
        return new Vector2i(position.x + size.x / 2, position.y + size.y / 2);
    }
    
    public static boolean containsPoint(final UIElement e, final int pX, final int pY){
        return containsPoint(e.viewPosition, e.size, pX, pY);
    }
    
    public static boolean containsPoint(final Vector2i position, final Vector2i size, final int x, final int y){
        return x >= position.x && x < (position.x + size.x) && y >= position.y && y < (position.y + size.y);
    }
    
    public boolean containsPoint(int x, int y){
        if(this.width == 0){
            throw new IllegalStateException("width cannot be null !");
        }
        if(this.width == 0){
            throw new IllegalStateException("height cannot be null !");
        }
        return x >= this.x && x < (this.x + this.width) && y >= this.y && y < (this.y + this.height);
    }
    
}
