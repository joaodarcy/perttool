/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.desenhar;

import br.com.joaodarcy.perttool.ResourceManager;
import static br.com.joaodarcy.perttool.ui.DrawUtils.*;
import java.awt.BasicStroke;
import java.awt.Stroke;

/**
 * Constantes utilizadas para desenhar assim como classes de traços a serem 
 * utilizados.
 */
public final class Constants {        
            
    public static ConnectorType DEFAULT_CONNECTOR = ConnectorType.CONNECTOR_TYPE_CUBIC_BEZIER;
    
    public final static int EVT_RADIUS          = 42;
    public final static int EVT_CONTAINER_WIDTH  = EVT_RADIUS + 16;
    public final static int EVT_CONTAINER_HEIGHT = EVT_RADIUS;
    public final static int EVENT_HALF_RADIUS   = EVT_RADIUS / 2;    
    public final static float EVT_HALF_RADIUS_FLOAT = (float)EVT_RADIUS / 2.0f;
    
    public final static float ARROW_IMAGE_WIDTH;
    public final static float ARROW_IMAGE_HALF_WIDTH;
    public final static float ARROW_IMAGE_HEIGTH;
    public final static float ARROW_IMAGE_HALF_HEIGTH;
    
    // Traços utilizados
    public final static Stroke ARROW_LINE_STROKE            = new BasicStroke(2.0f);
    public final static Stroke ARROW_LINE_PHANTHOM_STROKE   = new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, new float[]{5.0f}, 0.0f);
    
    static{
        ARROW_IMAGE_WIDTH = ResourceManager.ARROW_IMAGE.getWidth();
        ARROW_IMAGE_HALF_WIDTH = ResourceManager.ARROW_IMAGE.getWidth() / 2.0f;
        ARROW_IMAGE_HEIGTH = ResourceManager.ARROW_IMAGE.getHeight();
        ARROW_IMAGE_HALF_HEIGTH = ResourceManager.ARROW_IMAGE.getHeight() / 2.0f;
    }
    
}
