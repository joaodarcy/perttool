/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import br.com.joaodarcy.perttool.cmd.MoveEventCommand;
import br.com.joaodarcy.perttool.desenhar.Constants;
import br.com.joaodarcy.perttool.gui.EventContextMenu;
import br.com.joaodarcy.perttool.gui.GUIHelper;
import br.com.joaodarcy.perttool.ui.Rectangle;
import br.com.joaodarcy.perttool.ui.UIElement;
import br.com.joaodarcy.perttool.ui.UISystem;
import br.com.joaodarcy.perttool.math.Vector2i;
import static br.com.joaodarcy.perttool.ui.DrawUtils.ConnectorType;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;

/**
 * Is an event of network, have your unique ID, calculated runtime and one flag used for performing the calculation.
 */
public class Event extends UIElement {
    public final int id;
    private double earlyTime;
    private double lateStart;
    private double gap;
    private EventType type;

    private Vector2i dragBeginPos;
    private Vector2i dragEndPos;
    
    public Event(UISystem uiSystem, int id) {
        this(uiSystem, id, EventType.EVENT_TYPE_NORMAL);
    }
    
    public Event(UISystem uiSystem, int id, EventType type) {
        super(uiSystem);
        this.id = id;
        this.earlyTime = this.lateStart = this.gap = 0.0;
        this.type = type;
        this.moveParentOnDrag = true;
        this.draggable = false;
        
        this.dragBeginPos = new Vector2i();
        this.dragEndPos = new Vector2i();
        
        size.set(Constants.EVT_RADIUS, Constants.EVT_RADIUS);
    }
        
    public static boolean isEvent(UIElement e){
        return Event.class.isInstance(e);
    }
    
    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }   

    public double getEarlyTime() {
        return earlyTime;
    }

    public void setEarlyTime(double earlyTime) {
        this.earlyTime = earlyTime;
    }

    public double getLateStart() {
        return lateStart;
    }

    public void setLateStart(double lateStart) {
        this.lateStart = lateStart;
    }

    public double getGap() {
        return gap;
    }

    public void setGap(double gap) {
        this.gap = gap;
    }        
    
    public boolean hasReferenceActivities(){
        final PertDocument doc = Application.getDoc();
        for(Activity a : doc.activitiesList){
            if(a.eventEnd == id){
                return true;
            }
        }
        return false;
    }
    
    public boolean hasChildActivities(){
        final PertDocument doc = Application.getDoc();
        for(Activity a : doc.activitiesList){
            if(a.eventStart == id){
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void render(Graphics2D g) {
        FontMetrics metrics = g.getFontMetrics();              

        int x = viewPosition.getX();
        int y = viewPosition.getY();
        int width = size.getX();
        int height = size.getY();
                
        g.setColor(Color.WHITE);
        g.fillOval(x + 2, y + 2, 38, 38);
        
        Image drawImage = null;
        switch(type){
            case EVENT_TYPE_NORMAL: drawImage = ResourceManager.EVENT_IMAGE; break;
            case EVENT_TYPE_START: drawImage = ResourceManager.EVENT_START_IMAGE; break;
            case EVENT_TYPE_END: drawImage = ResourceManager.EVENT_END_IMAGE; break;
            default:
                throw new IllegalStateException("Unknow event type " + type);
        }
        
        g.drawImage(drawImage, x, y, width, height, null);
        
        // Draw the Event Number
        {
            String strIdEvento = String.valueOf(id + 1);
            int textWidth = metrics.stringWidth(strIdEvento);
            int textHeight = metrics.getDescent();

            Vector2i center = Rectangle.getCenter(this);
            int textX = center.getX() - textWidth / 2;
            int textY = center.getY() + textHeight;

            g.setColor(Color.BLACK);
            g.drawString(strIdEvento, textX, textY);
        }
                
        {
            final PertDocument doc = Application.getDoc();
            String strCriticalPath = String.format(doc.getEventWeightFormatString(),earlyTime, lateStart, gap);
            int textWidth = metrics.stringWidth(strCriticalPath);
            int textHeight = metrics.getHeight();
            
            Vector2i bottom = new Vector2i(x + width / 2, y + height);            
            int textX = bottom.getX() - textWidth / 2;
            int textY = bottom.getY() + textHeight;
            
            g.setColor(Color.BLACK);
            g.drawString(strCriticalPath, textX, textY);
        }
        
        super.render(g);
    }

    public void showDeleteDialog(){
        final PertDocument doc = Application.getDoc();
        if(JOptionPane.showConfirmDialog(doc, Internationalization.getMsgConfirmDeleteEvent(), Internationalization.getDeleteEvent(), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
            try{
                doc.deleteEvent(this);
            }catch(Throwable t){
                JOptionPane.showMessageDialog(doc, t.getMessage(), Internationalization.getDeleteEvent(), JOptionPane.ERROR_MESSAGE);                                
            }
        }
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_DELETE){
            showDeleteDialog();
        }
    }
    
    @Override
    protected void onDragBegin() {
        dragBeginPos.set(viewPosition.getX(), viewPosition.getY());
    }

    @Override
    protected void onDragEnd() {
        dragEndPos.set(viewPosition.getX(), viewPosition.getY());
        MoveEventCommand command = new MoveEventCommand(this, dragBeginPos, dragEndPos);
        Application.storeAndExecute(command);
    }
        
    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2){
            if(type == EventType.EVENT_TYPE_END){
                GUIHelper.showEditNetworkLateTime();
            }
        }else if(e.getButton() == MouseEvent.BUTTON3){
            EventContextMenu.showContextMenuFor(this, e.getX(), e.getY());
        }
        super.mousePressed(e);
    }        
    
    @Override
    public String toString() {
        return String.format("%s: %4d", Internationalization.getWordEvent(), id + 1);
    }            
}
