/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Ref. Introduction To Algorithms - Third Edition
 * 
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class DFSTopologySort implements TopologySort {
    
    protected void dfsTopologySort(PertDocument doc, int currentEvent, Stack<Integer> out, Map<Integer, Boolean> discovered){
        discovered.put(currentEvent, Boolean.TRUE);
        List<Activity> adjacentEdges = doc.getStartingActivitesInEvent(currentEvent);
        for(Activity activity : adjacentEdges){
            final int nextEvent = activity.eventEnd;
            if(discovered.get(nextEvent) == Boolean.FALSE){
                dfsTopologySort(doc, nextEvent, out, discovered);
            }
        }        
        out.push(currentEvent);
    }
    
    @Override
    public Stack<Integer> sort(PertDocument doc) {                
        Stack<Integer> out = new Stack<>();
        {
            Map<Integer, Boolean> discoveredMap = new HashMap<>();
            for(Integer i : doc.eventMap.keySet()){
                discoveredMap.put(i, Boolean.FALSE);
            }            
            dfsTopologySort(doc, doc.eventStart, out, discoveredMap);
        }                
        return out;
    }    
}
