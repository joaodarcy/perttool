/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import br.com.joaodarcy.perttool.cmd.DeleteActivityCommand;
import static br.com.joaodarcy.perttool.ui.DrawUtils.ConnectorType;
import br.com.joaodarcy.perttool.ui.UIElement;
import br.com.joaodarcy.perttool.ui.UISystem;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;

/**
 * Is an Activity of PERT Network, have weight, event start and event end of this Activity.
 */

public class Activity extends UIElement {
    
    private String description;
    public int eventStart;
    public int eventEnd;
    private double weight;
    private boolean criticalPath;
    private ConnectorType connectorType;
    
    private final PertDocument doc;
    
    /*public Activity(UISystem uiSystem, PertDocument doc, double weight, int eventStart, int eventEnd) {
        this(uiSystem, doc, weight, eventStart, eventEnd, ConnectorType.CONNECTOR_TYPE_LINE);
    }*/
    
    public Activity(UISystem uiSystem, PertDocument doc, double weight, int eventStart, int eventEnd, ConnectorType connectorType) {
        super(uiSystem);
        this.description = null;
        this.doc = doc;
        this.eventStart = eventStart;
        this.eventEnd = eventEnd;
        this.weight = weight;
        this.criticalPath = false;
        this.connectorType = connectorType;
    }

    public ConnectorType getConnectorType() {
        return connectorType;
    }

    public void setConnectorType(ConnectorType connectorType) {
        this.connectorType = connectorType;
    }

    public boolean hasDescription(){
        return description != null && !description.isEmpty();
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCriticalPath() {
        return criticalPath;
    }

    public void setCriticalPath(boolean criticalPath) {
        this.criticalPath = criticalPath;
    }              

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }        
    
    /**
     * Check if is a phantom Activity.
     * @return true if a phantom, and false if not.
     */    
    public boolean isPhatom(){
        return weight == 0.0;
    }

    @Override
    public void render(Graphics2D g) {
        // Do Nothing
    }                

    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2){
            showEditDialog();
        }
        super.mousePressed(e);
    }        
        
    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_DELETE){
            showDeleteDialog();
        }
    }        
    
    public void showEditDialog(){
        showEditDialog(false);
    }
    
    public void showEditDialog(boolean editingNewActivity){
        doc.showEditActivityDialogFor(this, editingNewActivity);
    }
    
    public void showDeleteDialog(){
        if(JOptionPane.showConfirmDialog(doc.getParent(), Internationalization.getConfirmDeleteActivityMessage(), Internationalization.getConfirmDeleteActivityTitle(), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
            DeleteActivityCommand command = new DeleteActivityCommand(this);
            Application.storeAndExecute(command);
        }else{
            uiSystem.setViewDirty();
        }
    }
    
    public static boolean isActivity(UIElement e){
        return Activity.class.isInstance(e);
    }
    
    @Override
    public String toString() {
        return String.format(
            "%s: %4d -> %s: %4d, %s: %f",             
            Internationalization.getStartEvent(),
            eventStart + 1, 
            Internationalization.getEndEvent(),
            eventEnd + 1,
            Internationalization.getWordWeight(), 
            weight
        );
    }       
}
