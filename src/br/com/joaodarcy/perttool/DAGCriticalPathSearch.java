/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Ref. Introduction To Algorithms - Third Edition
 *
 * Uses DFSTopologySort by default.
 * 
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class DAGCriticalPathSearch implements CriticalPathSearch {

    private final static double INFINITE = Double.MIN_VALUE;
    private final static int NIL = -1;
    
    private final TopologySort topologySort;

    public DAGCriticalPathSearch() {
        this(new DFSTopologySort());
    }
    
    public DAGCriticalPathSearch(TopologySort sort) {
        this.topologySort = sort;
    }       
    
    @Override
    public void findCriticalPath(PertDocument doc) {
        final int eventCount = doc.getEventCount();
        final int target = doc.eventEnd;
        
        // Relaxation Vars
        final int source = doc.eventStart;
        final Map<Integer, Double> weightMap = new HashMap<>(eventCount);
        final Map<Integer, Integer> previousEventMap = new HashMap<>(eventCount);
        final Map<Integer, Activity> criticalActivityMap = new HashMap<>(eventCount);       
        
        // Sort the Topology
        Stack<Integer> sorted = topologySort.sort(doc);
        
        // Relaxation
        for(Integer i : doc.eventMap.keySet()){
            weightMap.put(i, INFINITE);
            previousEventMap.put(i, NIL);
            criticalActivityMap.put(i, null);
        }
        weightMap.put(source, 0.0);
        
        while(!sorted.isEmpty()){
            int current = sorted.pop();
            if(current == target){
                break;
            }            
            List<Activity> adjacentEdges = doc.getStartingActivitesInEvent(current);
            for(Activity activity : adjacentEdges){
                final int nextEvent = activity.eventEnd;                
                // Relax
                final double currentWeight = weightMap.get(current);
                final double currentOption = currentWeight + activity.getWeight();
                final double nextEventWeight = weightMap.get(nextEvent);
                if(nextEventWeight < currentOption){
                    weightMap.put(nextEvent, currentOption);
                    previousEventMap.put(nextEvent, current);
                    criticalActivityMap.put(nextEvent, activity);
                }                                                
            }
        }

        // Update the Critical Path
        int u = target;
        Integer previousId = 0;
        while((previousId = previousEventMap.get(u)) != NIL){
            final Activity critical = criticalActivityMap.get(u);
            if(critical != null){
                critical.setCriticalPath(true);
            }
            u = previousId;
        }        
    }
    
}
