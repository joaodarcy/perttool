/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool;

import br.com.joaodarcy.perttool.cmd.AddActivityCommand;
import br.com.joaodarcy.perttool.desenhar.Constants;
import br.com.joaodarcy.perttool.math.Vector2f;
import br.com.joaodarcy.perttool.ui.DrawUtils;
import br.com.joaodarcy.perttool.ui.Rectangle;
import br.com.joaodarcy.perttool.ui.UIButton;
import br.com.joaodarcy.perttool.ui.UIElement;
import br.com.joaodarcy.perttool.ui.UISystem;
import br.com.joaodarcy.perttool.math.Vector2i;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class NewActivityButton extends UIButton {

    protected Event ourEvent;
    protected PertDocument doc;    
    protected Vector2i mouseTrackPos;
    
    protected boolean showTrack;
    protected boolean overEvent;
    
    public NewActivityButton(UISystem uiSystem, Event ourEvent, PertDocument doc) {
        super(uiSystem);
        this.ourEvent = ourEvent;
        this.doc = doc;
        this.mouseTrackPos = new Vector2i();
        
        // Settings
        this.draggable = false;
        this.overEvent = false;        
        this.size.set(16, 16);
        this.clientPosition.set(Constants.EVT_RADIUS, 0);
        this.imageNormal = ResourceManager.PLUS_IMAGE;
        this.imageOver = ResourceManager.PLUS_IMAGE_OVER;
    }

    @Override
    protected void onLoseFocus(UIElement elementGainFocus) {        
        if(showTrack && Event.isEvent(elementGainFocus) && elementGainFocus != ourEvent){
            AddActivityCommand command = new AddActivityCommand(doc, uiSystem, ourEvent.id, ((Event)elementGainFocus).id, 0.0, Constants.DEFAULT_CONNECTOR);
            Application.storeAndExecute(command);
            
            final Activity newActivity = command.getNewActivity();
            if(newActivity != null){
                newActivity.showEditDialog(true);
            }            
        }        
        showTrack = false;
    }        
    
    @Override
    protected void onMouseClick(MouseEvent e) {
        if(ourEvent.getType() != EventType.EVENT_TYPE_END){
            mouseTrackPos.set(e.getX(), e.getY());
            showTrack = true;
            overEvent = false;
        }
    }

    @Override
    public void render(Graphics2D g) {
        if(!doc.imageExporting && ourEvent.getType() != EventType.EVENT_TYPE_END){                    
            if(showTrack){
                Vector2f center = Rectangle.getCenterEx(parent);
                if(overEvent){
                    g.setColor(Color.GREEN);
                }else{
                    g.setColor(Color.RED);
                }
                DrawUtils.drawConnector(g, Constants.ARROW_LINE_PHANTHOM_STROKE, center, mouseTrackPos);
            }
            super.render(g);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        /*if(showTrack){
            mouseTrackPos.set(e.getX(), e.getY());
        }*/
        super.mouseDragged(e);
    }   
    
    @Override
    public void mouseMoved(MouseEvent e) {        
        if(showTrack){
            mouseTrackPos.set(e.getX(), e.getY());
            
            final UIElement mouseOver = uiSystem.getDesktop().getElementAt(e.getX(), e.getY());                        
            overEvent = Event.isEvent(mouseOver) && mouseOver != ourEvent;
            
            uiSystem.setViewDirty();
        }
        super.mouseMoved(e);
    }       
    
    public static boolean isNewActivityButton(UIElement e){
        return NewActivityButton.class.isInstance(e);
    }
    
}
