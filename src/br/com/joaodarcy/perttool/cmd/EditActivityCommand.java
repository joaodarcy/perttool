/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.cmd;

import br.com.joaodarcy.perttool.Activity;
import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.PertDocument;
import static br.com.joaodarcy.perttool.ui.DrawUtils.*;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class EditActivityCommand implements Command {
        
    private final int eventStart;
    private final int eventEnd;
    private final String oldDescription;
    private final String newDescription;
    private final double oldWeight;
    private final double newWeight;
    private final ConnectorType newConnectorType;
    private final ConnectorType oldConnectorType;

    public EditActivityCommand(Activity activity, String newDescription, double newWeight, ConnectorType newConnectorType ) {
        this.eventStart = activity.eventStart;
        this.eventEnd = activity.eventEnd;
        this.oldDescription = activity.getDescription();
        this.newDescription = newDescription;
        this.oldWeight = activity.getWeight();
        this.newWeight = newWeight;
        this.newConnectorType = newConnectorType;
        this.oldConnectorType = activity.getConnectorType();
    }

    @Override
    public void execute() {
        final PertDocument doc = Application.getDoc();
        final Activity activity = doc.getActivityByStartAndEndEvent(eventStart, eventEnd);
        
        if(activity == null){
            throw new IllegalStateException("Activity can't be null !");
        }
        
        activity.setConnectorType(newConnectorType);
        activity.setDescription(newDescription);
        activity.setWeight(newWeight);
                
        try{
            doc.recalculateNetworkTime();
        }catch(Throwable t){
            t.printStackTrace();
        }
    }

    @Override
    public void unExecute() {
        final PertDocument doc = Application.getDoc();
        final Activity activity = doc.getActivityByStartAndEndEvent(eventStart, eventEnd);
        
        if(activity == null){
            throw new IllegalStateException("Activity can't be null !");
        }
        
        activity.setConnectorType(oldConnectorType);
        activity.setDescription(oldDescription);
        activity.setWeight(oldWeight);
        
        try{
            doc.recalculateNetworkTime();
        }catch(Throwable t){
            t.printStackTrace();
        }
    }    
}
