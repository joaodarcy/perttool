/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.cmd;

import br.com.joaodarcy.perttool.Activity;
import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.Internationalization;
import br.com.joaodarcy.perttool.PertDocument;
import static br.com.joaodarcy.perttool.ui.DrawUtils.*;
import br.com.joaodarcy.perttool.ui.UISystem;
import javax.swing.JOptionPane;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class AddActivityCommand implements Command {
        
    private final PertDocument doc;
    private final UISystem uiSystem;
    private final int srcEventId;
    private final int dstEventId;
    private double weight;
    private String description;
    private ConnectorType connectorType;
    
    private Activity newActivity;

    public AddActivityCommand(int srcEventId, int dstEventId, double weight, ConnectorType connectorType) {        
        this(Application.getDoc(), Application.getDoc().getUiSystem(), srcEventId, dstEventId, weight, connectorType);
    }
    
    public AddActivityCommand(PertDocument doc, UISystem uiSystem, int srcEventId, int dstEventId, double weight, ConnectorType connectorType) {
        this.doc = doc;
        this.uiSystem = uiSystem;
        this.srcEventId = srcEventId;
        this.dstEventId = dstEventId;
        this.weight = weight;
        this.description = null;
        this.newActivity = null;
        this.connectorType = connectorType;
    }

    public Activity getNewActivity() {
        return newActivity;
    }
                    
    @Override
    public void execute() {        
        newActivity = new Activity(uiSystem, doc, weight, srcEventId, dstEventId, connectorType);
        try{
            newActivity.setDescription(description);
            doc.addActivity(newActivity, true);            
        }catch(Throwable t){
            newActivity = null;
            JOptionPane.showMessageDialog(doc, t.getMessage(), Internationalization.getAppTitle(), JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void unExecute() {
        final Activity activityToDelete = newActivity;                
        if(activityToDelete == null){
            return;
        }
        newActivity = null;
        weight = activityToDelete.getWeight();
        description = activityToDelete.getDescription();
        doc.delActivity(activityToDelete);
    }    
}
