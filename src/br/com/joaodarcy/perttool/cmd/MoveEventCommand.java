/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2016  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.cmd;

import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.Event;
import br.com.joaodarcy.perttool.PertDocument;
import br.com.joaodarcy.perttool.math.Vector2i;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class MoveEventCommand implements Command {

    private final PertDocument doc;
    private final Event event;
    private final int deltaX;
    private final int deltaY;
    private boolean firstMove;

    public MoveEventCommand(Event event, Vector2i oldPosition, Vector2i newPosition) {
        this.doc = Application.getDoc();
        this.event = event;
        this.deltaX = newPosition.getX() - oldPosition.getX();
        this.deltaY = newPosition.getY() - oldPosition.getY();
        this.firstMove = true;
    }                   
    
    @Override
    public void execute() {
        if(firstMove){
            firstMove = false;
            return;
        }
        event.moveAmount(deltaX, deltaY);
        doc.repaint();
    }

    @Override
    public void unExecute() {
        event.moveAmount(deltaX * -1, deltaY * -1);
        doc.repaint();
    }
    
}
