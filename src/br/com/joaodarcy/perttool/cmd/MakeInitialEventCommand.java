/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.cmd;

import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.Event;
import br.com.joaodarcy.perttool.EventType;
import br.com.joaodarcy.perttool.Internationalization;
import br.com.joaodarcy.perttool.PertDocument;
import javax.swing.JOptionPane;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class MakeInitialEventCommand implements Command {

    private final int oldInitialEventId;
    private final int newInitialEventId;

    public MakeInitialEventCommand(int newInitialEventId) {
        final PertDocument doc = Application.getDoc();
        this.oldInitialEventId = doc.eventStart;
        this.newInitialEventId = newInitialEventId;
    }
    
    public static boolean canMakeInitial(Event event){
        final PertDocument doc = Application.getDoc();
        if(event.hasReferenceActivities()){
            JOptionPane.showMessageDialog(doc, Internationalization.getInitialEventCantHaveReferencingActivities(), Internationalization.getMakeInitialEvent(), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;        
    }
    
    protected void makeInitial(int newInitial, int oldInitial){
        final PertDocument doc = Application.getDoc();
        final Event event = doc.getEventById(newInitial);        
        final Event startEvent = doc.getEventById(oldInitial);
        // Swap Final with Initial                
        if(event.getType() == EventType.EVENT_TYPE_END){                                        
            doc.eventEnd = startEvent.id;
            doc.eventStart = event.id;

            startEvent.setType(EventType.EVENT_TYPE_END);
            event.setType(EventType.EVENT_TYPE_START);
        }else{                    
            doc.eventStart = event.id;
            startEvent.setType(EventType.EVENT_TYPE_NORMAL);
            event.setType(EventType.EVENT_TYPE_START);
        }
        try{
            doc.recalculateNetworkTime();
        }catch(Throwable t){
            t.printStackTrace();
        }
        doc.repaint();
    }
    
    @Override
    public void execute() {
        makeInitial(newInitialEventId, oldInitialEventId);
    }

    @Override
    public void unExecute() {
        makeInitial(oldInitialEventId, newInitialEventId);
    }    
}
