/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.cmd;

import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.Event;
import br.com.joaodarcy.perttool.EventType;
import br.com.joaodarcy.perttool.Internationalization;
import br.com.joaodarcy.perttool.PertDocument;
import javax.swing.JOptionPane;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class MakeFinalEventCommand implements Command {

    private final int oldFinalEventId;
    private final int newFinalEventId;

    public MakeFinalEventCommand(int newFinalEventId) {
        final PertDocument doc = Application.getDoc();
        this.oldFinalEventId = doc.eventEnd;
        this.newFinalEventId = newFinalEventId;
    }
    
    public static boolean canMakeFinal(Event event){
        final PertDocument doc = Application.getDoc();
        if(event.hasChildActivities()){
            JOptionPane.showMessageDialog(doc, Internationalization.getFinalEventCantHaveChildActivites(), Internationalization.getMakeFinalEvent(), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;        
    }
    
    protected void makeFinal(int newFinal, int oldFinal){
        final PertDocument doc = Application.getDoc();
        final Event event = doc.getEventById(newFinal);
        final Event endEvent = doc.getEventById(doc.eventEnd);
        // Swap Initial with Final
        if(event.getType() == EventType.EVENT_TYPE_START){                
            doc.eventStart = endEvent.id;
            doc.eventEnd = event.id;

            endEvent.setType(EventType.EVENT_TYPE_START);
            event.setType(EventType.EVENT_TYPE_END);
        }else{                    
            doc.eventEnd = event.id;
            endEvent.setType(EventType.EVENT_TYPE_NORMAL);
            event.setType(EventType.EVENT_TYPE_END);
        }
        try{
            doc.recalculateNetworkTime();
        }catch(Throwable t){
            t.printStackTrace();
        }
        doc.repaint();
    }
            
    @Override
    public void execute() {
        makeFinal(newFinalEventId, oldFinalEventId);
    }

    @Override
    public void unExecute() {
        makeFinal(oldFinalEventId, newFinalEventId);
    }    
}
