/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.cmd;

import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.PertDocument;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class AddEventCommand implements Command {

    private final PertDocument doc;
    private final int newEventId;

    public AddEventCommand() {
        this.doc = Application.getDoc();
        this.newEventId = doc.findNextAvailableEventId();
    }
            
    @Override
    public void execute() {
        doc.addNewEvent(newEventId);
    }

    @Override
    public void unExecute() {
        doc.deleteEventById(newEventId);
    }    
}
