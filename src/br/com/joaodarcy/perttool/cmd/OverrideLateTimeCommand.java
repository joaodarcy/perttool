/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.cmd;

import br.com.joaodarcy.perttool.Application;
import br.com.joaodarcy.perttool.PertDocument;
import br.com.joaodarcy.perttool.PertException;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class OverrideLateTimeCommand implements Command {

    private final double oldLateTime;
    private final boolean oldLateTimeState;
    private final double newLateTime;
    private final boolean newLateTimeState;

    public OverrideLateTimeCommand(double oldLateTime, boolean oldLateTimeState, double newLateTime, boolean newLateTimeState) {
        this.oldLateTime = oldLateTime;
        this.oldLateTimeState = oldLateTimeState;
        this.newLateTime = newLateTime;
        this.newLateTimeState = newLateTimeState;
    }
            
    @Override
    public void execute() {
        final PertDocument doc = Application.getDoc();
        doc.setOverrideLateTimeValue(newLateTime);
        doc.setOverrideLateTime(newLateTimeState);
        try{
            doc.recalculateNetworkTime();
        }catch(PertException e){
            e.printStackTrace();
        }
    }

    @Override
    public void unExecute() {
        final PertDocument doc = Application.getDoc();
        doc.setOverrideLateTimeValue(oldLateTime);
        doc.setOverrideLateTime(oldLateTimeState);
        try{
            doc.recalculateNetworkTime();
        }catch(PertException e){
            e.printStackTrace();
        }
    }
    
}
