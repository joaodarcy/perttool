/**
 *  PERTTool <https://gitlab.com/joaodarcy/perttool>
 *  Copyright (C) 2014-2015  João Darcy Tinoco Sant´Anna Neto
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.joaodarcy.perttool.cmd;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author João Darcy Tinoco Sant´Anna Neto <jdtsncomp@gmail.com>
 */
public class MacroCommand implements Command {      
    
    protected LinkedList<Command> commandList;

    public MacroCommand() {
        this.commandList = new LinkedList<>();
    }
    
    public void add(Command command){
        commandList.add(command);
    }
    
    public void remove(Command command){
        commandList.remove(command);
    }
    
    @Override
    public void execute() {
        for(Command command : commandList){
            command.execute();
        }
    }

    @Override
    public void unExecute() {
        // unExecute in reverse order
        Iterator<Command> reverseIterator = commandList.descendingIterator();
        while(reverseIterator.hasNext()){
            reverseIterator.next().unExecute();
        }
    }
            
}
